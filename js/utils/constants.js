const BASE_PATH = 'http://api.meego.co';
const NEW_BASE_PATH = `http://178.62.101.168:3000`;
const BASE_PATH_GUIDE = 'http://api.meego.co/portal/guides'

const SERVER_URL = `${BASE_PATH}/user`;
const NEW_SERVER_URL = `http://178.62.101.168:3000/user`;

const FACEBOOK_APP_ID = '133198930667849';
const GOOGLE_IOS_EXPO = '1411413842-ra3l3kpjjd6tl64199biekov3qdfgq7u.apps.googleusercontent.com';
const GOOGLE_ANDROID_EXPO = '1411413842-56r5bcv2jo8ev1llit2lfuk9qa8c8mg5.apps.googleusercontent.com';
const GOOGLE_IOS_STANDALONE = '1411413842-6q1uhll6lamkc27jn9psuka4t8gkolrd.apps.googleusercontent.com';
const GOOGLE_ANDROID_STANDALONE = '1411413842-idi197oss4mpf0ejst0qp36qq598tdmm.apps.googleusercontent.com';
const GOOGLE_WEB_STANDALONE = '1411413842-h1lepe4u2v90ut5921b0qndmfvvnogug.apps.googleusercontent.com';

const SENTRY_DSN = 'https://9e82a5ec3f2347859c25319aed618edb@sentry.io/233264';

const SEGMENT_IOS_KEY = '86C1wexUYCiUs9xwwHENEWzgbdI7SKu3';
const SEGMENT_ANDROID_KEY = 'lDiEuzdPGEpUPm1yoJBCdv3N0YXOjq1X';

const GOOGLE_MAPS_API_KEY = 'AIzaSyBh_VKN456wD_wnB9bewzxdfmJvCv6opq0';

const RAZORPAY_KEY_ID = 'rzp_test_38rXqc2qbSuPbe';

export {
    FACEBOOK_APP_ID,
    GOOGLE_IOS_EXPO,
    GOOGLE_ANDROID_EXPO,
    GOOGLE_IOS_STANDALONE,
    GOOGLE_ANDROID_STANDALONE,
    GOOGLE_WEB_STANDALONE,
    BASE_PATH,
    SERVER_URL,
    NEW_SERVER_URL,
    NEW_BASE_PATH,
    SENTRY_DSN,
    SEGMENT_IOS_KEY,
    SEGMENT_ANDROID_KEY,
    GOOGLE_MAPS_API_KEY,
    RAZORPAY_KEY_ID
};
