import { Observable } from 'rxjs';

function DialogEvent() {
    this.observable = Observable.create(observer => {
        this.observer = observer;
    });
    this.subscription = null;
    this.responseResolve = null;
}

DialogEvent.prototype.alert = function (dialogTitle, dialogBody, closeText = 'Okay', showCancel = false, cancelText = 'Cancel') {
    return new Promise(resolve => {
        this.responseResolve = resolve;
        this.observer.next({
            dialogTitle, dialogBody, closeText, showCancel, cancelText
        });
    });
};

DialogEvent.prototype.subscribe = function (cb) {
    this.subscription = this.observable.subscribe(({
        dialogTitle, dialogBody, closeText, showCancel, cancelText
    }) => {
        cb(dialogTitle, dialogBody, closeText, showCancel, cancelText);
    });
};

DialogEvent.prototype.choose = function (confirm) {
    this.responseResolve(confirm);
};

DialogEvent.prototype.confirm = function () {
    this.choose(true);
};

DialogEvent.prototype.cancel = function () {
    this.choose(false);
};

DialogEvent.prototype.dispose = function () {
    this.subscription.unsubscribe();
};

export default new DialogEvent();
