import axios from 'axios';
import { SERVER_URL,NEW_SERVER_URL,NEW_BASE_PATH } from '../utils/constants';
import { handleError } from './login';


const updateLocation = (id, latitude, longitude) => new Promise((resolve, reject) => {
    // console.log('api: ',latitude, longitude );
    // axios.post(`${SERVER_URL}/update-location`, {
    // // axios.post(`http://178.62.101.168:3000/portal/guides`, {
    //     id: id || undefined,
    //     latitude,
    //     longitude
    // })
    //     .then(response => {
    //         // console.log('user location resp',response.data);
    //         // response.fo
    //         console.log(response.data);
    //         resolve(response.data);
    //     })
    //     .catch(err => {
    //         console.log('eer',err);
    //         handleError(err, reject);
    //     });
    // console.log(`${NEW_SERVER_URL}/update-location`);
        
        axios.get(`${NEW_BASE_PATH}/portal/guides?latitude=${latitude}&longitude=${longitude}&radius=50000`)
            .then(response => {
                // response.fo
                response.data.map((item)=>{
                        item.distance="2.45 km",
                        item.duration="5 min",
                        item.longitude=(item.longitude)?item.longitude: 76.78,
                        item.latitude=(item.latitude)?item.latitude:30.753,
                        item.type="food",                        
                        // item.image="/"+item.image,                        
                        item.image=(item.image)?"/"+item.image:"/images/nearby/b2c87590-2cb3-11e8-b70c-59ab69d669fb.jpeg"

                })
                resolve({
                    "atm":[],
                    "food": response.data,
                    "monument": [],
                    "popular":[]


                });
            })
            .catch(err => {                
                handleError(err, reject);
            });    
});

const requestCall = (id, phone, language, latitude, longitude) => new Promise((resolve, reject) => {
    axios.get(`${SERVER_URL}/call-booking`, {
        params: {
            id,
            phone,
            language,
            latitude,
            longitude
        }
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const getCallHistory = (id, offset) => new Promise((resolve, reject) => {
    axios.get(`${SERVER_URL}/call-history`, {
        params: {
            id,
            offset
        }
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});


export { updateLocation, requestCall, getCallHistory };
