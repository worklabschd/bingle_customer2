import axios from 'axios';
import { SERVER_URL,NEW_SERVER_URL } from '../utils/constants';
import { handleError } from './login';

const updateProfilePicture = (id, avatar) => new Promise((resolve, reject) => {
    console.log('updateProfilePicture',`${NEW_SERVER_URL}/edit-avatar`,id);
    axios.post(`${NEW_SERVER_URL}/edit-avatar`, {
        id,
        avatar
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const updateProfile = (id, profile) => new Promise((resolve, reject) => {
    console.log('update profile',`${NEW_SERVER_URL}/edit-profile`,id);
    axios.post(`${NEW_SERVER_URL}/edit-profile`, {
        id,
        ...profile
    })
        .then(response => {
            console.log('profile_data',response.data);
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

export { updateProfilePicture, updateProfile };
