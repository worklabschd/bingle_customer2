/**
 * Created by M on 17/02/17. With ❤
 */

import axios from 'axios';
import Polyline from 'polyline';

import { GOOGLE_MAPS_API_KEY, BASE_PATH_GUIDE } from '../utils/constants';

export function autocompleteFromInput(valueToSearch, latitude, longitude) {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios.get(`https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${GOOGLE_MAPS_API_KEY}&input=${valueToSearch}&location=${latitude},${longitude}&radius=100000`);
            const serialized = response.data;
            if (serialized.status === 'OK') {
                const { predictions } = serialized;
                if (predictions.length < 1) {
                    resolve([]);
                    return;
                }
                const result = predictions.map(place => (
                    {
                        name: place.structured_formatting.main_text,
                        address: place.structured_formatting.secondary_text || '',
                        placeid: place.place_id
                    }
                ));
                resolve(result);
                return;
            }
            reject(serialized.status);
        } catch (ex) {
            reject(ex);
        }
    });
}

export function getLatlongFromPlaceID(placeid) {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios.get(`https://maps.googleapis.com/maps/api/place/details/json?key=${GOOGLE_MAPS_API_KEY}&placeid=${placeid}`);
            const serialized = response.data;
            if (serialized.status === 'OK') {
                const latitude = serialized.result.geometry.location.lat;
                const longitude = serialized.result.geometry.location.lng;
                resolve({ latitude, longitude });
                return;
            }
            reject(serialized.status);
        } catch (ex) {
            reject(ex);
        }
    });
}

export function getGuide(){
    fetch('http://api.meego.co/portal/guides')  
        .then(function(response) {            
        //    console.log(response.json())
        })
    
}

export function getPolylineLatlongsAndTripInfo(
    sourceLat,
    sourceLong,
    destinationLat,
    destinationLong
) {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios.get(`https://maps.googleapis.com/maps/api/directions/json?origin=${sourceLat},${sourceLong}&destination=${destinationLat},${destinationLong}&units=metric&key=${GOOGLE_MAPS_API_KEY}`);
            const serialzed = response.data;
            if (serialzed.status === 'OK') {
                const { distance, duration } = serialzed.routes[0].legs[0];
                const overview_polyline = serialzed.routes[0].overview_polyline.points; // eslint-disable-line
                const latLongs = Polyline
                    .decode(overview_polyline)
                    .map(latlong => ({ latitude: latlong[0], longitude: latlong[1] }));
                resolve({
                    distanceString: distance.text,
                    distance: distance.value,
                    time: duration.value,
                    latLongs
                });
            } else {
                reject(serialzed.status);
            }
        } catch (ex) {
            reject(ex);
        }
    });
}
