import axios from 'axios';
import { SERVER_URL } from '../utils/constants';
import { handleError } from './login';

const getDue = (id) => new Promise((resolve, reject) => {
    axios.get(`${SERVER_URL}/payment-due`, {
        params: {
            id
        }
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const makePayment = (id, paymentID, amount, paymentGateway) => new Promise((resolve, reject) => {
    axios.post(`${SERVER_URL}/make-payment`, {
        id,
        paymentID,
        amount,
        paymentGateway
    })
        .then(() => {
            resolve();
        })
        .catch(err => {
            handleError(err, reject);
        });
});


export { getDue, makePayment };
