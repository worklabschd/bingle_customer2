import SnackBar from 'react-native-snackbar-dialog';
import { color } from '../theme';

const displayToast = (message, position = 'bottom') => {
    SnackBar.show(message, {
        position,
        duration: 3500,
        tapToClose: true,
        textColor: 'white',
        confirmText: 'Okay',
        buttonColor: color.aqua,
        onConfirm: () => {
            SnackBar.dismiss();
        }
    });
};

const displayNetworkError = () => {
    displayToast('Experiencing network issues. Please try again.');
};

const displayInternalServer = () => {
    displayToast('Internal server error. Please try again.');
};

export { displayToast, displayNetworkError, displayInternalServer };
