import React from 'react';
import PropTypes from 'prop-types';
import { View, Keyboard, Platform, StyleSheet, TouchableWithoutFeedback } from 'react-native';

class KeyboardListener extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyboardShown: false
        };
    }

    componentWillMount() {
        if (Platform.OS === 'ios') {
            Keyboard.addListener('keyboardWillShow', this.keyboardShown.bind(this));
            Keyboard.addListener('keyboardWillHide', this.keyboardHidden.bind(this));
        }
    }

    componentWillUnmount() {
        if (Platform.OS === 'ios') {
            Keyboard.removeAllListeners('keyboardWillShow');
            Keyboard.removeAllListeners('keyboardWillHide');
        }
    }

    keyboardShown() {
        this.setState({ keyboardShown: true });
    }


    keyboardHidden() {
        this.setState({ keyboardShown: false });
    }

    render() {
        return (
            <View style={styles.app} >
                {this.props.children}
                { Platform.OS === 'ios' && this.state.keyboardShown ?
                    <TouchableWithoutFeedback
                        onPress={Keyboard.dismiss}
                        style={styles.keyboardDismissView}
                    >
                        <View style={styles.keyboardDismissView} />
                    </TouchableWithoutFeedback>
                    :
                    null
                }
            </View>
        );
    }
}

KeyboardListener.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array
    ]).isRequired
};

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    keyboardDismissView: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 999
    }
});

export default KeyboardListener;
