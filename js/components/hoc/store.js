/* eslint react/prop-types: "off" */

import React from 'react';
import { Provider } from 'react-redux';
import store from '../../reducers';

export default (props) => (
    <Provider store={store}>
        {props.children}
    </Provider>
);
