import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash'
import { View, StyleSheet, Text, Platform,Image,ScrollView,Dimensions } from 'react-native';
import { HamContainer } from '../../common';
import { color, font } from '../../theme';
import { Ionicons } from '@expo/vector-icons';
import placeReviews from '../../../api/placeReviews.json';


this.state = {
    reviews : placeReviews.reviews,
};

const PlaceReviews = ({ navigation: { goBack, state: { params: { photos,place } } } }) => {
    //console.log('place Data : ',place);
    return (
    <HamContainer
        title=" "
        backEnabled
        navigate={goBack}
    >
        <View style={{padding: 12,paddingBottom:0}}>
            <Text style={{fontFamily: font.primary_mont_bold, fontSize: 24}}>{place.name}</Text>
            <Text style={{fontFamily: font.primary_mont_medium, fontSize: 20,color:color.darkgrey}}>{place.state}</Text>
            <Text style={{margin:10, fontFamily: font.primary_mont_medium, fontSize: 22,color:color.aqua}}>All Reviews</Text>
        </View>
        <View style={styles.container}>
            <ScrollView style={{flexDirection:'column', marginBottom:0, paddingBottom:0 }} >
                
            {this.state.reviews.map((review, index) =>

                <View key={index} style={[styles.reviewContainer, {marginVertical:5,}]}>
                    <View style={{padding:12, flexDirection:'row', justifyContent:'flex-start'}}>
                        <View style={{borderRadius:60, overflow: 'hidden'}}>
                        <Image source={{uri:review.imageURL}}
                            style={{height:60, width:60}} resizeMode='stretch' />
                        </View>
                        <Text style={{textAlignVertical:'center', marginHorizontal:5, fontFamily: font.primary_mont_bold, fontSize: 18, textAlign:'center'}}>
                        {review.userName}</Text>    
                    </View>

                    <View style={{paddingHorizontal:12, marginVertical:10, alignItems:'center', flexDirection:'row', justifyContent:'space-between'}}>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text style={{fontFamily: font.primary_mont_semi_bold, fontSize: 12,color:color.darkgrey}}>
                                RATED</Text>
                            <View style={styles.starContainer} >
                                <Text style={styles.starText}>{review.rating} <Ionicons name="md-star" size={13} color={color.darkgrey} /></Text>
                            </View>
                        </View>
                        <View>
                            <Text style={{fontFamily: font.primary_mont_semi_bold, fontSize: 12,color:color.darkgrey}}>
                                {review.time}</Text>    
                        </View>    
                    </View>
                    <View style={{paddingHorizontal:12, flexDirection:'row'}}>
                        <Text style={{fontFamily: font.primary_mont_semi_bold, fontSize: 15,}}>
                            {review.desc}
                        </Text>
                    </View>
                </View>
            )}

            </ScrollView>
        </View>
    </HamContainer>
    )
};


const styles = StyleSheet.create({
    reviewContainer:{
        flexDirection:'column',
        //elevation: 5,
        //position:"relative",
        //backgroundColor: "#fff",
        //borderRadius:3,
        //shadowColor: color.aqua,
        //overflow:"hidden"
        borderBottomWidth:2, borderBottomColor:color.aqua, borderTopWidth:2, borderTopColor:color.aqua
    },
    container: {
      flex: 1,
    },
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
    starText:{
        color: color.darkgrey,
        fontSize: 13,
        fontFamily: font.primary_mont_medium
    },
    starContainer: {
        width: 45,
        height: 30,
        marginHorizontal:5,
        borderRadius: 20,
        borderColor: color.darkgrey,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 8
    }
  })

export default PlaceReviews;