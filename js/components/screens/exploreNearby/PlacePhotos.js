import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash'
import { View, StyleSheet, Text, Platform,Image,ScrollView,Dimensions } from 'react-native';
import { HamContainer } from '../../common';
import { color, font } from '../../theme';
const windowWidth = Dimensions.get('window').width;
var IMAGES_PER_ROW = 3;
const calculatedSize = () => {
    var size = windowWidth / IMAGES_PER_ROW
    return {width: size, height: size}
  };

const PlacePhotos = ({ navigation: { goBack, state: { params: { photos,place } } } }) => {
    return (
    <HamContainer
        title="Photos"
        backEnabled
        navigate={goBack}
    >
    <View style={{padding: 20,paddingBottom:0}}>
    <Text style={{fontFamily: font.primary_mont_bold, fontSize: 24}}>{place.name}</Text>
    <Text style={{fontFamily: font.primary_mont_medium, fontSize: 20,color:color.grey}}>{place.state}</Text>
    </View>
    <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <View >
          {renderImagesIngroups(IMAGES_PER_ROW,photos)}
          </View>
        </ScrollView>
</View>
    </HamContainer>
    )
};


  

const renderImagesIngroups = (imagesPerRow,ImageData) => {
    const imageSize = calculatedSize()
    const images = _.chunk(ImageData, imagesPerRow).map((imgObj, idx) => (
      <View key={`row-${idx}`} style={styles.row}>
        {
          imgObj.map((img, i) => (
            <Image
                key={i}
                source={{uri: img.url}}
                style={{margin:5 , padding:5,width:imageSize.width - 20, height:imageSize.height - 20,borderRadius:4}}
            />
          ))
        }
      </View>
    ))
    return (
      <View>
        {images}
      </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
    },
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
  })

export default PlacePhotos;