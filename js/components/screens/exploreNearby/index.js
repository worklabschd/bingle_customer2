import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Dimensions, Image, StyleSheet, ScrollView, Platform, TouchableOpacity,ImageBackground,StatusBar,Linking } from 'react-native';
import AnimatedTabs from 'react-native-animated-tabs';
import { HamContainer } from '../../common';
import { color, font } from '../../theme';
import { BASE_PATH, NEW_BASE_PATH } from '../../../utils/constants';
import { Ionicons } from '@expo/vector-icons';
import { Badge } from 'react-native-elements';
import { MapView, Location } from 'expo';
import StarRating from 'react-native-star-rating';
// import RatingStar from 'react-native-rating-star';

const { height, width } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0195;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;



let photos = [
    {
        "url": "https://www.gstatic.com/webp/gallery/1.sm.jpg"
    },
    {
        "url": "https://www.gstatic.com/webp/gallery/1.sm.jpg"
    },
    {
        "url": "https://www.gstatic.com/webp/gallery/1.sm.jpg"
    },
    {
        "url": "https://www.gstatic.com/webp/gallery/1.sm.jpg"
    }
]
function onStarRatingPress(rating) {
    // this.setState({
    //   starCount: rating
    // });
}
const Tab = ({
    name,
    description,
    place,
    distance,
    image,
    latitude,
    longitude,
    navigation

}) => (
        <View
            key={name}
            style={styles.container}
        >
            <ScrollView
                ref={ref => this.scrollView = ref}
                >
                <View style={styles.imageContainer} >
                    <ImageBackground
                        source={{ uri: `${NEW_BASE_PATH}${image}` }}
                        style={{
                            height: 200,
                        }}
                    >
                    <TouchableOpacity style={{left:20,top:20}} onPress={ () => navigation.goBack() }>
                    <Ionicons name="ios-arrow-back" size={50} color="#00D6ff"/>
                    </TouchableOpacity>
                    </ImageBackground>

                </View>
                <View style={styles.tabsContainer} >
                    <TouchableOpacity onPress={() => navigation.navigate('PlacePhotos', { photos, place })} style={styles.iconButton}>
                        <Text style={[styles.iconButtonText,{textDecorationLine: 'underline'}]}>Photos</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.scrollView.scrollToEnd({animated: true})} style={styles.iconButton}>
                        <Text style={[styles.iconButtonText,{textDecorationLine: 'underline'}]}>Map</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('PlaceReviews', { photos, place })} style={styles.iconButton}>
                        <Text style={[styles.iconButtonText,{textDecorationLine: 'underline'}]}>Review</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.descriptionContainer}>
                    <View style={styles.nameContainer}>
                        <Text style={styles.name} numberOfLines={2}>{name}</Text>
                        <View style={{ flex: 1 }} />
                        <Badge
                            value={3.5}
                            containerStyle={{ backgroundColor: '#9ACD32' }}
                            textStyle={{ color: color.white, fontFamily: font.primary_mont_medium }}
                        />
                    </View>

                    <View style={[styles.nameContainer]}>
                        <Text style={{fontFamily: font.primary_mont_medium, fontSize: 15, color: color.darkgrey}}>{place.state}</Text>
                    </View>

                    <View style={[styles.nameContainer]}>
                        <Text style={styles.iconButtonText}>Open Now</Text>
                        <Text style={{color:color.darkgrey,fontSize:16}}> • </Text>
                        <Text style={{fontFamily: font.primary_mont_medium, fontSize: 15, color: color.darkgrey,marginTop:1}}>9AM To 11PM (Today)</Text>
                    </View>

                    <TouchableOpacity onPress={() => navigation.navigate('PlaceRating', { place })} >
                    <View style={[styles.nameContainer, { marginTop: 10 }]}>
                        <Text style={{fontFamily: font.primary_mont_bold, fontSize: 20, color: '#07213d'}} numberOfLines={2}>Rate this place</Text>
                    </View>
                    

                    <View style={{ flex: 1, flexDirection: 'row',marginTop:10 }}>
                        <View style={styles.starContainer} >
                            <Text style={styles.starText}>1 <Ionicons name="md-star" size={13} color={color.darkgrey} /></Text>
                        </View>
                        <View style={styles.starContainer} >
                            <Text style={styles.starText}>2 <Ionicons name="md-star" size={13} color={color.darkgrey} /></Text>
                        </View>
                        <View style={styles.starContainer} >
                            <Text style={styles.starText}>3 <Ionicons name="md-star" size={13} color={color.darkgrey} /></Text>
                        </View>
                        <View style={styles.starContainer} >
                            <Text style={styles.starText}>4 <Ionicons name="md-star" size={13} color={color.darkgrey} /></Text>
                        </View>
                        <View style={styles.starContainer} >
                            <Text style={styles.starText}>5 <Ionicons name="md-star" size={13} color={color.darkgrey} /></Text>
                        </View>
                    </View>
                    </TouchableOpacity>

                    <View style={[styles.nameContainer, { marginTop: 15 }]}>
                        <Text style={styles.address}>CALL</Text>
                    </View>

                    <View style={[styles.nameContainer]}>
                        <Text style={{color:'#099e44',fontSize:16,fontFamily: font.primary_mont_medium}}>{place.countryCode} {place.phone}</Text>
                        <View style={{ flex: 1 }} />
                        <TouchableOpacity onPress={()=> {callNumber(`tel:${place.countryCode}${place.phone}`)}}><Ionicons name="md-call" size={20} color='#099e44' /></TouchableOpacity>
                    </View>

                    <View style={[styles.nameContainer, { marginTop: 10 }]}>
                        <Text style={styles.address}>ADDRESS</Text>
                    </View>

                    <View style={[styles.nameContainer,{flex:1}]}>
                        <Text style={styles.description}>{place.state} , {place.pinCode}</Text>
                    </View>

                    <View style={[styles.nameContainer, { marginTop: 10 }]}>
                        <Text style={{fontFamily: font.primary_mont_bold, fontSize: 20, color: '#07213d'}} numberOfLines={2}>Photos</Text>
                    </View>

                    <View style={[styles.nameContainer, { marginTop: 10 }]}>
                        {
                            photos.map((item, i) => {
                                return (
                                    <View key={i} style={styles.imagesView}>
                                        <Image
                                            source={{ uri: item.url }}
                                            style={{
                                                height: 60,
                                                width: 65,
                                                borderRadius:5
                                            }} />
                                    </View>
                                )
                            })
                        }
                    </View>


                    <View style={[styles.nameContainer, { height: 250 }]}>
                        <MapView
                            // ref={map => { this.map = map; }}
                            provider="google"
                            style={{ flex: 1, height }}
                            loadingEnabled
                            // loadingIndicatorColor={color.primary}
                            initialRegion={{
                                latitude: latitude,
                                longitude: longitude,
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA,
                            }}
                        // onRegionChangeComplete={this.onChangeLatLongBind}
                        >
                            {Platform.OS === 'ios' ?
                                <MapView.Marker
                                    coordinate={{
                                        latitude: 30.753,
                                        longitude: 30.753
                                    }}
                                    image={require('../../../../assets/icons/small_pin.png')}
                                // image={require('../../../../assets/icons/own_location_small.png')}
                                />
                                :
                                <MapView.Marker
                                    coordinate={{
                                        latitude: 30.753,
                                        longitude: 30.753
                                    }}
                                    image={require('../../../../assets/icons/small_pin.png')}
                                />
                            }
                        </MapView>
                    </View>
                </View>
            </ScrollView>

        </View>

    );

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

function callNumber(url){
    Linking.canOpenURL(url).then(supported => {
        if (!supported) {
         console.log('Can\'t handle url: ' + url);
        } else {
         return Linking.openURL(url);
        }
      }).catch(err => console.error('An error occurred', err));
}

const exploreNearby = ({ navigation, navigation: { goBack, state: { params: { place } } } }) => (
        <Tab
            name={place.name}
            place={place}
            description={place.description}
            distance={place.distance}
            image={place.image}
            latitude={place.latitude}
            longitude={place.longitude}
            navigation={navigation}
        />

);

Tab.propTypes = {
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    distance: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
};

exploreNearby.propTypes = {
    navigation: PropTypes.shape({
        goBack: PropTypes.func,
        state: PropTypes.shape({
            params: PropTypes.shape({})
        })
    }).isRequired
};

const styles = StyleSheet.create({
    container: {
        height: height,
        width: width,
        flexDirection: 'column',
        backgroundColor: color.white
    },
    imageContainer: {
        flex: 0.5,
    },
    tabsContainer: {
        flex:0.6,
        paddingLeft:20,
        paddingTop:5,
        flexDirection: 'row',
    },
    iconButton: {
        flex:0.2,
        alignItems: 'center'
    },
    descriptionContainer: {
        flex: 1.1,
        padding: 15
    },
    nameContainer: {
        flexDirection: 'row',
        paddingBottom: 5
    },
    name: {
        fontFamily: font.primary_mont_bold,
        fontSize: 24,
        color: '#07213d'
    },
    address: {
        fontFamily: font.primary_mont_medium,
        fontSize: 12,
        color: color.darkgrey
    },
    description: {
        fontFamily: font.primary_mont_medium,
        fontSize: 15,
        flex:0.7
    },
    iconButtonText: {
        color: '#00D6ff',
        fontFamily: font.primary_mont_semi_bold,
        fontSize: 15,
    },
    imagesView: {
        paddingRight: 10
    },
    starText:{
        color: color.darkgrey,
        fontSize: 13,
        fontFamily: font.primary_mont_medium
    },
    starContainer: {
        width: 45,
        height: 30,
        borderRadius: 20,
        borderColor: color.darkgrey,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 8
    }
});

export default exploreNearby;
