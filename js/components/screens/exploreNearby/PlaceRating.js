import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ListView,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Text,
  StatusBar,
  StyleSheet,
  Platform,
  DrawerNavigator,
  TouchableHighlight,
  FlatList,
  ActivityIndicator,
  Alert,
  Image, TextInput,
} from 'react-native';
import { HamContainer } from '../../common';
import { color, font } from '../../theme';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import StarRating from 'react-native-star-rating';

class PlaceRating extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        title: null,
        review: null,
        starCount: 0
    }
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  render() {

    return (

      <View style={{flex:1, flexDirection:'column'}}>
        <View style={{height:23, backgroundColor:'transparent'}}></View>
        <View style={{height:60, alignItems:'center', flexDirection:'row', justifyContent:'space-between', backgroundColor:'transparent'}}>
            
            <View style={{alignItems:'flex-start'}}>
            <TouchableOpacity
                hitSlop={{top: 30, bottom: 30, left: 30, right: 30}}    
                style={{ marginLeft: 10 }}
                onPress={ () => this.props.navigation.goBack() }
                >
                <Text style={{fontSize:15, fontFamily:font.primary_mont_semi_bold, color:color.aqua}}>
                    Cancel</Text>
            </TouchableOpacity>
            </View>

            <View style={{alignItems:'center',}}>
            <Text style={{fontSize:24, fontFamily:font.primary_mont_semi_bold }}>Rate Us</Text>
            </View>

            <View style={{alignItems:'flex-end'}}>
                <TouchableOpacity
                    hitSlop={{top: 30, bottom: 30, left: 30, right: 30}}                
                    style={{ marginRight: 10 }}
                    >
                    <Text style={{fontSize:15, fontFamily:font.primary_mont_semi_bold, color:color.aqua}}>
                        Send</Text>
                </TouchableOpacity>
            </View>

        </View>
        <View style={styles.profileContainer}>
                <View style={{ borderRadius:60, overflow:'hidden'}}>
                  <Image source={require('../../../../assets/icons/avatar.jpg')}
                      style={{marginHorizontal:20, marginVertical:5, height:60, width:60,}} resizeMode='stretch'/>
                </View>    
                <Text style={{textAlignVertical:'center', marginHorizontal:5, fontFamily: font.primary_mont_semi_bold, fontSize: 24, textAlign:'center'}}>
                    Sandeep</Text>    
        </View>
        <View style={{flex:1,}}>

            <View style={{flexDirection:'column', marginTop:20, alignItems:'center'}}>
                <StarRating
                    starSize={25}
                    containerStyle={{width:150, }}
                    disabled={false}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    rating={this.state.starCount}
                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                    fullStarColor={color.aqua}
                />
                <Text style={{fontSize:12, color:color.darkgrey, fontFamily:font.primary_mont_regular, }}>Tap a star to Rate</Text>
            </View>
                    <TextInput
                            style={styles.Inputs}
                            onChangeText={(text) => this.setState({title:text})}
                            value={this.state.title}
                            placeholder='Title'
                            placeholderColor={color.darkgrey}
                            underlineColorAndroid='transparent'
                    />
                    <TextInput
                            style={styles.Inputs}
                            onChangeText={(text) => this.setState({review:text})}
                            value={this.state.review}
                            placeholder='Review (Optional)'
                            placeholderColor={color.darkgrey}
                            underlineColorAndroid='transparent'
                            multiline={true}
                            numberOfLines={10}
                            
                    />
        </View>

      </View>
    );
  }
}

// PlaceRating.propTypes = {
//   navigation: PropTypes.shape({
//       goBack: PropTypes.func,
//   }).isRequired
// };

const styles = StyleSheet.create({
  profileContainer:{
    flexDirection:'row', justifyContent:'flex-start',
    borderBottomWidth:2, borderBottomColor:color.aqua, borderTopWidth:2, borderTopColor:color.aqua
  },
  Inputs :{
    marginHorizontal:10, 
    height:60,
    fontSize:18, 
    fontFamily:font.primary_mont_semi_bold,  
    borderTopWidth:1, borderTopColor:color.grey,
    textAlignVertical:'center', 
    borderColor: 'transparent',
    
  }
  })

export default PlaceRating;
