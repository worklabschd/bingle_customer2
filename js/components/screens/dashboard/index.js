import React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Modal,
  TextInput,
  TouchableOpacity,
  Animated,
  Image,
  Keyboard,
  Easing,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  ScrollView,
  ActivityIndicator
} from "react-native";

import { MapView, Location, Permissions, Notifications } from "expo";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import { Badge } from "react-native-elements";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as Animatable from "react-native-animatable";
import { NavigationActions } from "react-navigation";
import * as _ from "lodash";
import { Dropdown } from "react-native-material-dropdown";
import { setLocation } from "../../../actions";
import { color, font } from "../../theme";
import { NearbyPlace, HamContainer } from "../../common";
import { Snackbar } from "../../widgets";
import { PlacesAPI, BookingAPI, Constants } from "../../../api";
import Events from "../../../utils/events";
import { DialogEvent, LoadingEvent } from "../../../events";
import { BASE_PATH, NEW_BASE_PATH } from "../../../utils/constants";

const { height, width } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0195;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const languageDate = [
  {
    value: "English"
  },
  {
    value: "Hindi"
  },
  {
    value: "French"
  }
];

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    const {
      user: { phone },
      latitude,
      longitude
    } = props;

    this.state = {
      fadeAnim: new Animated.Value(height - width / 1.6),
      open: false,
      modalVisible: false,
      balanceModal: false,
      language: "English",
      number: phone || "",
      show: false,
      text: "",
      placeIndex: -1,
      marker: {
        latitude,
        longitude
      },

      cardPadded: false,
      nearbyVisible: false,
      places: [],

      nearbyPlaces: {
        popular: [],
        monument: [],
        food: [],
        atm: []
      },

      nearbyPlacesLoading: false,
      nearbyChoice: "food",
      notification: "",
      showNearbyOnMap: true,
      showPolyline: false,
      polyline: [],
      balance: ""
    };

    this.open = false;
    this.initialRegion = {};
    this.resetButtonShown = false;
    this.callButtonRotate = new Animated.Value(0);

    this.callButtonRotateValue = this.callButtonRotate.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "180deg"]
    });

    this.resetPinBind = this.resetPin.bind(this);
    this.onChangeLatLongBind = this.onChangeLatLong.bind(this);
    this.onPlaceTypedBind = this.onPlaceTyped.bind(this);
    this.getPlacesDebounced = _.debounce(this.getPlaces.bind(this), 300);
    this.callBind = this.call.bind(this);
    this.showNearbyOnMapBind = this.showNearbyOnMap.bind(this);
    this._handleNotification = this._handleNotification.bind(this);
    this.goToPaymentsBind = this.goToPayments.bind(this);
  }

  async componentDidMount() {
    PlacesAPI.getGuide();

    if (Platform.OS !== "ios") {
      this.keyboardDidShowListener = Keyboard.addListener(
        "keyboardDidShow",
        () => this.setState({ cardPadded: true })
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        "keyboardDidHide",
        () => this.setState({ cardPadded: false })
      );
    }
    const { latitude, longitude } = this.props;
    this.setState({ text: "" });

    const reversCode = await Location.reverseGeocodeAsync({
      latitude: this.props.latitude,
      longitude: this.props.longitude
    });

    console.log(reversCode);
    this.setState({
      text: reversCode[0].city + ", " + reversCode[0].region
    });

    if (latitude === 0.0 && longitude === 0.0) {
      await DialogEvent.alert(
        "Oops!",
        "It looks your phone doesn't have access to your location. Go inside a better coverage area or enable location and try again.",
        "Try Again"
      );
      this.resetPin();
      return;
    }
    this.fetchNearby();
    setTimeout(() => {
      if (Platform.OS === "ios") {
        this.animateMapToRegion(latitude, longitude);
      }
    }, 1000);
    this.registerForPushNotificationsAsync();
  }

  async registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== "granted") {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== "granted") {
      return;
    }

    // Get the token that uniquely identifies this device
    const token = await Notifications.getExpoPushTokenAsync();
    // POST the token to your backend server from where you can retrieve it to send push notifications.
    // return fetch(PUSH_ENDPOINT, {
    //     method: 'POST',
    //     headers: {
    //         Accept: 'application/json',
    //         'Content-Type': 'application/json',
    //     },
    //     body: JSON.stringify({
    //         token: {
    //             value: token,
    //         },
    //         user: {
    //             username: 'Brent',
    //         },
    //     }),
    // });
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
  }

  _handleNotification(notification) {
    this.setState({ notification });
  }

  componentWillUnmount() {
    if (Platform.OS !== "ios") {
      this.keyboardDidShowListener.remove();
      this.keyboardDidHideListener.remove();
    }
  }

  async fetchNearby() {
    const { latitude, longitude } = this.state.marker;
    // console.log(latitude, longitude, "latitude, longitude");
    this.setState({
      nearbyPlacesLoading: true
    });

    try {
      console.log("update location", latitude, longitude);
      const nearbyPlaces = await BookingAPI.updateLocation(
        this.props.user.id,
        latitude,
        longitude
      );
      console.log("nearbyPlaces", nearbyPlaces);
      this.setState({
        nearbyPlaces,
        nearbyPlacesLoading: false
      });
    } catch (err) {
      this.setState({
        nearbyPlacesLoading: false
      });
      console.log(err);
    }
  }

  //Fetch the explore api

  onChangeLatLong(region) {
    console.log("chnage load map", region);
    if (!this.started && Platform.OS === "ios") {
      this.started = true;
      return;
    }
    if (!this.initialRegion.latitude) {
      this.initialRegion = region;
      this.resetButtonShown = false;
      this.resetButton.zoomOut(350);
      return;
    }
    if (
      this.initialRegion.latitude === region.latitude &&
      this.initialRegion.longitude === region.longitude
    ) {
      this.resetButtonShown = false;
      this.resetButton.zoomOut(350);
      return;
    }
    if (!this.resetButtonShown) {
      this.resetButton.zoomIn(350);
      this.resetButtonShown = true;
    }
  }

  onPlaceTyped(text) {
    //console.log('search Text : ', text);
    this.setState({ text });
    if (text.length > 2) {
      this.getPlacesDebounced(text);
    } else {
      this.setState({
        places: []
      });
    }
  }

  async onPlaceChosen(place) {
    this.setState({
      show: false,
      text: `${place.name} ${place.address}`
    });
    this._root.blur();

    const { latitude, longitude } = await PlacesAPI.getLatlongFromPlaceID(
      place.placeid
    );

    this.setState({
      marker: {
        latitude,
        longitude
      }
    });

    this.animateMapToRegion(latitude, longitude);
    this.fetchNearby();
    Events.track("Location chosen from drop down.", {
      address: place.address,
      name: place.name
    });
  }

  async getPlaces(text) {
    const places = await PlacesAPI.autocompleteFromInput(
      text,
      this.state.marker.latitude,
      this.state.marker.longitude
    );
    //console.log('getplaces: ', places);
    this.setState({
      places
    });
  }

  setModalVisible(visible) {
    // If tarrif/language modal is not open.
    if (!this.state.open) {
      if (visible) {
        Animated.timing(this.callButtonRotate, {
          toValue: 1,
          duration: 450,
          easing: Easing.linear
        }).start();
        setTimeout(() => {
          this.setState({ modalVisible: visible });
        }, 460);
        return;
      }
      Animated.timing(this.callButtonRotate, {
        toValue: 0,
        duration: 350,
        easing: Easing.linear
      }).start();
      this.setState({ modalVisible: visible, cardPadded: false });
      Events.track("Call button pressed.");
    }
  }

  setBalanceModalVisible(visible) {
    this.setState({
      balanceModal: visible
    });
  }

  setNearbyModalVisible(nearbyVisible, index) {
    this.setState({
      nearbyVisible,
      placeIndex: index
    });
  }

  animateMapToRegion(latitude, longitude) {
    if (this.map) {
      this.map.animateToRegion(
        {
          latitude,
          longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        },
        1500
      );
    }
  }

  async resetPin() {
    const {
      locationServicesEnabled,
      gpsAvailable
    } = await Location.getProviderStatusAsync();
    const {
      coords: { latitude, longitude }
    } = await Location.getCurrentPositionAsync({
      enableHighAccuracy:
        this.props.permissionGiven &&
        locationServicesEnabled &&
        (Platform.OS === "ios" || gpsAvailable),
      maximumAge: 100000
    });
    this.setState({
      text: "",
      marker: {
        latitude,
        longitude
      },
      showNearbyOnMap: false,
      showPolyline: false,
      polyline: []
    });
    this.initialRegion = {};
    this.props.setLocation(latitude, longitude);
    this.animateMapToRegion(latitude, longitude);
    this.fetchNearby();
  }

  openTarrif() {
    if (!this.state.open) {
      Animated.timing(this.state.fadeAnim, {
        toValue: height - width / 1.1,
        duration: 400
      }).start();
      this.setState({ open: !this.state.open });
      Events.track("Tarrif modal opened.");
    } else {
      Animated.timing(this.state.fadeAnim, {
        toValue: height - height / 2.9,
        duration: 400
      }).start();
      this.setState({ open: !this.state.open });
    }
  }

  changePlace(nearbyChoice) {
    Object.keys(this.state.nearbyPlaces).forEach(place => {});

    this.setState({
      nearbyChoice
    });
    console.log(nearbyChoice);
  }

  goToExploreNearby(placeIndex) {
    this.setState(
      {
        nearbyVisible: false
      },
      () => {
        const { nearbyPlaces, nearbyChoice } = this.state;
        this.props.navigation.navigate("ExploreNearby", {
          places: nearbyPlaces[nearbyChoice],
          placeIndex
        });
      }
    );
  }

  async call() {
    const {
      user: { id }
    } = this.props;
    if (!id) {
      this.goToLogin();
      return;
    }
    const {
      number,
      marker: { latitude, longitude },
      language
    } = this.state;
    const phone = number.trim();
    if (phone.length < 10) {
      Snackbar.displayToast(
        "Number doesn't seem to be valid. Please try again.",
        "top"
      );
      return;
    }
    this.setModalVisible(!this.state.modalVisible);
    const confirmed = await DialogEvent.alert(
      "Proceed?",
      "Are you sure you want to proceed to have a call with our expert?",
      "Yes!",
      true
    );
    if (!confirmed) {
      return;
    }
    setTimeout(() => LoadingEvent.show(), 300);
    try {
      const { message } = await BookingAPI.requestCall(
        id,
        phone,
        language.toLowerCase(),
        latitude,
        longitude
      );
      const [messageSplit, balance] = message.split("|");
      setTimeout(() => LoadingEvent.cancel(), 400);
      if (messageSplit === Constants.BALANCE_PENDING) {
        setTimeout(async () => {
          this.setState({
            balanceModal: true,
            balance
          });
        }, 700);
        return;
      }
      if (messageSplit === Constants.NO_GUIDE_AVAILABLE) {
        setTimeout(
          () =>
            DialogEvent.alert(
              "Yikes!",
              "Sadly, there is no expert available for this language in your area right now. Please be patient and try again later. But do not despair, we're adding new experts each day."
            ),
          700
        );
        return;
      }
      setTimeout(
        () =>
          DialogEvent.alert(
            "Success!",
            "Your booking with Meego has been registered. You'll shortly receive a call from our expert. Thank you for using Meego."
          ),
        700
      );
    } catch (err) {
      setTimeout(() => LoadingEvent.cancel(), 400);
      if (err === Constants.INTERNAL_SERVER_ERROR) {
        Snackbar.displayInternalServer();
        return;
      }
      Snackbar.displayNetworkError();
    }
  }

  goToLogin() {
    const action = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Login" })]
    });
    this.props.navigation.dispatch(action);
  }

  goToPayments() {
    this.props.navigation.navigate("Payments");
  }

  showNearbyOnMap() {
    this.setState(
      {
        showNearbyOnMap: true
      },
      () => {
        this.setNearbyModalVisible(false);
        const { nearbyChoice, nearbyPlaces } = this.state;

        const coords = _.map(
          nearbyPlaces[nearbyChoice],
          ({ latitude, longitude }) => ({
            latitude,
            longitude
          })
        );

        this.map.fitToCoordinates(
          coords,
          {
            edgePadding: {
              top: 50,
              bottom: 50,
              right: 20,
              left: 20
            }
          },
          true
        );
      }
    );
  }

  async showDirections(destinationLatitude, destinationLongitude) {
    LoadingEvent.show();
    const {
      marker: { latitude, longitude }
    } = this.state;
    const { latLongs } = await PlacesAPI.getPolylineLatlongsAndTripInfo(
      latitude,
      longitude,
      destinationLatitude,
      destinationLongitude
    );
    LoadingEvent.cancel();
    this.setState(
      {
        nearbyVisible: false,
        polyline: latLongs,
        showPolyline: true
      },
      () => {
        this.map.fitToCoordinates(
          [...latLongs, { latitude, longitude }],
          {
            edgePadding: {
              top: 150,
              bottom: 100,
              right: 40,
              left: 40
            }
          },
          true
        );
      }
    );
  }

  renderNearbyOnMap() {
    const { nearbyPlaces, nearbyChoice } = this.state;
    return nearbyPlaces[nearbyChoice].map(
      (
        { name, latitude, longitude, distance, duration, description },
        index
      ) => (
        <MapView.Marker
          key={index}
          coordinate={{ latitude, longitude }}
          // pinColor={color.maroon},
          title={name}
          description={description}
          onPress={() => this.setNearbyModalVisible(true, index)}
          image={require("../../../../assets/icons/small_pin.png")}
        >
          <MapView.Callout tooltip />
        </MapView.Marker>
      )
    );
  }

  renderNearbyList() {
    const {
      nearbyPlaces,
      nearbyChoice,
      nearbyPlacesLoading,
      placeIndex
    } = this.state;
    if (placeIndex == -1) {
      return false;
    }
    let selectedPlace = nearbyPlaces[nearbyChoice][placeIndex];
    if (selectedPlace) {
      return (
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: "row", margin: 10 }}>
            <Image
              style={{
                width: 60,
                height: 60,
                backgroundColor: color.grey,
                borderRadius: 10
              }}
              source={{ uri: `${NEW_BASE_PATH}${selectedPlace.image}` }}
            />
            <View style={{ flex: 1, flexDirection: "column", marginLeft: 10 }}>
              <Text
                style={{ fontFamily: font.primary_mont_medium, fontSize: 18 }}
              >
                {selectedPlace.name}
              </Text>
              <Text style={{ marginTop: 5, color: color.grey }}>
                {selectedPlace.state}
              </Text>
            </View>
            <Badge
              value={3.5}
              containerStyle={{ backgroundColor: "#9ACD32" }}
              textStyle={{ color: color.white }}
            />
          </View>
          <View style={{ flexDirection: "row", margin: 10 }}>
            <Text style={{ fontFamily: font.primary_mont, fontSize: 16 }}>
              Open Now
            </Text>
            <View style={{ flex: 1 }} />
            <Text style={{ fontFamily: font.primary_mont, padding: 5 }}>
              {" "}
              9AM to 11PM (Today)
            </Text>
          </View>
          <View style={{ flexDirection: "row", margin: 10 }}>
            <Text
              style={{
                fontFamily: font.primary_mont_medium,
                color: color.grey,
                fontSize: 14
              }}
            >
              Address
            </Text>
          </View>
          <View style={{ flexDirection: "column", margin: 10, marginTop: 5 }}>
            <Text style={{ fontFamily: font.primary_mont_medium }}>
              {selectedPlace.state}
            </Text>
            <Text
              style={{ fontFamily: font.primary_mont_medium, marginTop: 5 }}
            >
              {selectedPlace.pinCode}
            </Text>
          </View>
          <View
            style={{
              margin: 5,
              borderBottomColor: color.grey,
              borderBottomWidth: 1
            }}
          />
          <TouchableOpacity
            onPress={() =>
              this.showDirections(
                selectedPlace.latitude,
                selectedPlace.longitude
              )
            }
          >
            <View style={{ flexDirection: "row", margin: 10 }}>
              <Text
                style={{ fontFamily: font.primary_mont_medium, fontSize: 18 }}
              >
                <MaterialIcons
                  name="directions"
                  size={18}
                  color={color.black}
                />{" "}
                Directions
              </Text>
              <View style={{ flex: 1 }} />
              <Ionicons name="ios-arrow-forward" size={18} color={color.grey} />
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  }

  render() {
    return (
      <HamContainer
        title="Nearby"
        navigate={this.props.navigation.navigate}
        iconDisplay="ios-list-box-outline"
        iconColor
        rightIcon={{
          nearbyPlaces: this.state.nearbyPlaces,
          nearbyChoice: this.state.nearbyChoice,
          nearbyPlacesLoading: this.state.nearbyPlacesLoading,
          selectedLocation: this.state.text
        }}
      >
        <View style={styles.container}>
          <MapView
            ref={map => {
              this.map = map;
            }}
            provider="google"
            style={{ alignSelf: "stretch", width, height }}
            loadingEnabled
            loadingIndicatorColor={color.primary}
            initialRegion={{
              latitude: this.props.latitude,
              longitude: this.props.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
            onRegionChangeComplete={this.onChangeLatLongBind}
          >
            {Platform.OS === "ios" ? (
              <MapView.Marker
                coordinate={this.state.marker}
                image={require("../../../../assets/icons/user_map_pin.png")}
                // image={require('../../../../assets/icons/own_location_small.png')}
              />
            ) : (
              <MapView.Marker
                coordinate={this.state.marker}
                image={require("../../../../assets/icons/user_map_pin.png")}
              />
            )}
            {this.state.showNearbyOnMap ? this.renderNearbyOnMap() : null}
            {this.state.showPolyline ? (
              <MapView.Polyline
                strokeWidth={2}
                lineCap="round"
                lineJoin="round"
                strokeColor={color.aqua}
                coordinates={this.state.polyline}
              />
            ) : null}
          </MapView>
          <View style={{ position: "absolute" }}>
            <View
              style={{
                width: width - width / 8,
                top: 10,
                height: width / 8,
                borderRadius: width / 16,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                marginHorizontal: width / 16,
                backgroundColor: "rgba(255,255,255,.90)",
                borderColor: color.grey,
                borderWidth: 1
              }}
            >
              <TextInput
                style={{
                  flex: 1,
                  paddingLeft: width / 16,
                  paddingRight: width / 16,
                  fontFamily: font.primary_mont,
                  fontSize: 16
                }}
                underlineColorAndroid="transparent"
                placeholder="Enter Location"
                placeholderTextColor="#000000"
                value={this.state.text}
                onChangeText={this.onPlaceTypedBind}
                onFocus={() => this.setState({ show: true })}
                blurOnSubmit
                ref={component => (this._root = component)}
              />
              {/* <MaterialCommunityIcons
                            name="google-maps"
                            color={color.yellow}
                            size={30}
                            style={{ right: width / 20 }}
                        /> */}
            </View>
            {this.state.show &&
            this.state.places.length > 0 &&
            this.state.text.length > 2 ? (
              <View
                style={{
                  backgroundColor: "rgba(255,255,255,.9)",
                  borderRadius: 16,
                  top: height / 12,
                  alignSelf: "center",
                  width: width - width / 8,
                  marginTop: 15,
                  borderColor: color.grey,
                  borderWidth: 1
                }}
              >
                {this.state.places.map(place => (
                  <View
                    style={{
                      flexDirection: "column",
                      height: height / 10,
                      width: width - width / 10
                    }}
                    key={place.placeid}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.onPlaceChosen(place);
                      }}
                      style={{
                        height: height / 10 - 1,
                        width: width - width / 10,
                        paddingHorizontal: 15,
                        justifyContent: "center",
                        paddingVertical: 10
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: font.primary_mont
                        }}
                        ellipsizeMode="tail"
                        numberOfLines={2}
                      >
                        {`${place.name} ${place.address}`}
                      </Text>
                    </TouchableOpacity>
                    <View
                      style={{ backgroundColor: "#dedede", height: 1, flex: 1 }}
                    />
                  </View>
                ))}
              </View>
            ) : null}
            <View
              style={{
                top: height - width / 1.7,
                bottom: 0,
                backgroundColor: "transparent",
                height: width / 4,
                flexDirection: "row",
                justifyContent: "space-around",
                alignItems: "center"
              }}
            >
              {/* <TouchableOpacity
                            style={
                                {
                                    backgroundColor: 'transparent', width: width / 5.8, borderRadius: width / 12.1, height: width / 5.8, justifyContent: 'center', alignItems: 'center', left: 20
                                }}
                            onPress={() => this.setNearbyModalVisible(true)}
                        >
                            <Image source={require('../../../../assets/icons/compass_icon.png')} style={{ width: width / 5.8, height: width / 5.8 }} />
                        </TouchableOpacity> */}
              <TouchableOpacity
                style={{
                  backgroundColor: "transparent",
                  marginHorizontal: 10,
                  width: width / 4.8,
                  borderRadius: width / 9.6,
                  height: width / 4.8,
                  justifyContent: "center",
                  alignItems: "center"
                }}
                onPress={this.setModalVisible.bind(this, true)}
              >
                <Animated.Image
                  source={require("../../../../assets/icons/call_button.png")}
                  style={{
                    width: width / 4.8,
                    height: width / 4.8,
                    transform: [{ rotate: this.callButtonRotateValue }]
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: "transparent",
                  width: width / 6.1,
                  borderRadius: width / 12.1,
                  height: width / 6.1,
                  justifyContent: "center",
                  alignItems: "center",
                  right: 20
                }}
                onPress={this.resetPinBind}
              >
                <Animatable.Image
                  animation="zoomIn"
                  ref={component => {
                    this.resetButton = component;
                  }}
                  source={require("../../../../assets/icons/location_icon.png")}
                  style={{ width: width / 8.5, height: width / 8.5 }}
                />
              </TouchableOpacity>
            </View>
          </View>
          <Modal
            animationType="slide"
            transparent
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setModalVisible(!this.state.modalVisible);
            }}
          >
            <TouchableWithoutFeedback
              onPress={() => this.setModalVisible(!this.state.modalVisible)}
            >
              <KeyboardAvoidingView
                style={{
                  position: "relative",
                  top: 0,
                  left: 0,
                  bottom: 0,
                  right: 0,
                  flex: 1,
                  justifyContent: "flex-end"
                }}
              >
                <TouchableWithoutFeedback onPress={() => {}}>
                  <View
                    style={{
                      bottom: this.state.cardPadded
                        ? height === 568
                          ? height / 2
                          : height / 10
                        : height / 20,
                      backgroundColor: "rgba(255,255,255,.90)",
                      width: width - width / 8,
                      minHeight: width / 1.3,
                      alignSelf: "center",
                      alignItems: "center",
                      borderColor: color.grey,
                      borderWidth: 1,
                      borderRadius: 16
                    }}
                  >
                    <View style={{}}>
                      <View>
                        <Text
                          style={{
                            paddingTop: 13,
                            textAlign: "center",
                            fontFamily: font.primary_mont_regular,
                            fontSize: 13,
                            color: color.darkgrey
                          }}
                        >
                          Tariff
                        </Text>
                        <Text
                          style={{
                            fontFamily: font.primary_mont_medium,
                            fontSize: 25,
                            textAlign: "center"
                          }}
                        >
                          ₹ 19 / 3mins *
                        </Text>
                        <Text
                          style={{
                            textAlign: "center",
                            fontFamily: font.primary_mont_regular,
                            fontSize: 11,
                            color: color.darkgrey,
                            paddingBottom: 13
                          }}
                        >
                          You can pay by any of the payment methods later.
                        </Text>
                      </View>
                      <View
                        style={{
                          borderColor: color.grey,
                          borderWidth: 1,
                          borderLeftWidth: 0,
                          borderRightWidth: 0,
                          width: width / 2,
                          alignSelf: "center"
                        }}
                      >
                        <Dropdown
                          label="Language"
                          data={languageDate}
                          value={this.state.language}
                          inputContainerStyle={{
                            borderBottomColor: "transparent"
                          }}
                          onChangeText={language =>
                            this.setState({
                              language
                            })
                          }
                        />
                      </View>
                      <Text
                        style={{
                          paddingTop: 13,
                          textAlign: "center",
                          fontFamily: font.primary_mont_regular,
                          fontSize: 13,
                          color: color.darkgrey
                        }}
                      >
                        Change number before Call
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <TextInput
                          style={{
                            fontFamily: font.primary_mont_medium,
                            fontSize: 25,
                            paddingBottom: 13,
                            width: "80%",
                            textAlign: "center"
                          }}
                          keyboardType="phone-pad"
                          returnKeyType="done"
                          maxLength={10}
                          value={this.state.number}
                          placeholder="Enter Number"
                          underlineColorAndroid="transparent"
                          onChangeText={text => this.setState({ number: text })}
                          onFocus={() => this.setState({ cardPadded: true })}
                          onBlur={() => this.setState({ cardPadded: false })}
                        />
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "flex-end",
                          backgroundColor: "transparent"
                        }}
                      >
                        <TouchableOpacity
                          style={{
                            backgroundColor: this.props.user.id
                              ? color.green
                              : color.mediumGrey,
                            width: "100%",
                            height: width / 7,
                            justifyContent: "center",
                            alignItems: "center",
                            marginBottom: 10,
                            marginHorizontal: -15,
                            borderRadius: width / 14,
                            borderColor: color.grey,
                            borderWidth: 1
                          }}
                          onPress={this.callBind}
                        >
                          <Text
                            style={{
                              fontSize: 21,
                              fontFamily: font.primary_mont_regular
                            }}
                          >
                            {this.props.user.id
                              ? "Talk to Guide"
                              : "Signup to Proceed"}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
          </Modal>

          <Modal
            animationType="slide"
            transparent
            visible={this.state.nearbyVisible}
            onRequestClose={() => this.setNearbyModalVisible(false)}
          >
            <TouchableWithoutFeedback
              onPress={() => this.setNearbyModalVisible(false)}
            >
              <View
                style={{
                  position: "relative",
                  top: 0,
                  left: 0,
                  bottom: 0,
                  right: 0,
                  flex: 1,
                  justifyContent: "flex-end"
                }}
              >
                <View
                  style={{
                    bottom: 0,
                    backgroundColor: "rgba(255,255,255,.90)",
                    height: width / 1.3,
                    padding: 10
                  }}
                >
                  <View style={{ flex: 1 }}>
                    <ScrollView>{this.renderNearbyList()}</ScrollView>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
          <Modal
            animationType="slide"
            transparent
            visible={this.state.balanceModal}
            onRequestClose={() =>
              this.setBalanceModalVisible(!this.state.balanceModal)
            }
          >
            <TouchableWithoutFeedback
              onPress={() =>
                this.setBalanceModalVisible(!this.state.balanceModal)
              }
            >
              <KeyboardAvoidingView
                style={{
                  position: "relative",
                  top: 0,
                  left: 0,
                  bottom: 0,
                  right: 0,
                  flex: 1,
                  justifyContent: "flex-end"
                }}
              >
                <TouchableWithoutFeedback onPress={() => {}}>
                  <View
                    style={{
                      bottom: height / 20,
                      backgroundColor: "rgba(255,255,255,.90)",
                      width: width - width / 5,
                      minHeight: width / 2.25,
                      alignSelf: "center",
                      alignItems: "center",
                      borderColor: color.grey,
                      borderWidth: 1,
                      borderRadius: 16
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "column",
                        justifyContent: "space-between",
                        alignItems: "center",
                        padding: 10
                      }}
                    >
                      <View>
                        <Text
                          style={{
                            fontFamily: font.primary_mont_medium,
                            fontSize: 25,
                            textAlign: "center"
                          }}
                        >
                          ₹ {this.state.balance}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: width / 1.5,
                          alignSelf: "center"
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: font.primary_mont_medium,
                            fontSize: 14,
                            paddingVertical: 20,
                            textAlign: "center"
                          }}
                        >
                          You have some outstanding balance to pay. Please pay
                          to proceed.
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "column",
                          justifyContent: "center",
                          alignItems: "center",
                          width: width / 2,
                          backgroundColor: "transparent"
                        }}
                      >
                        <TouchableOpacity
                          style={{
                            backgroundColor: color.green,
                            width: "100%",
                            height: 40,
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: width / 14,
                            borderColor: color.grey,
                            borderWidth: 1
                          }}
                          onPress={this.goToPaymentsBind}
                        >
                          <Text
                            style={{
                              fontSize: 21,
                              fontFamily: font.primary_mont_regular,
                              marginTop: 0,
                              paddingVertical: 4
                            }}
                          >
                            Pay
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
      </HamContainer>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1989e8"
  },
  exploreNearby: {
    fontFamily: font.primary_mont,
    fontSize: 16
  },
  categoryContainer: {
    paddingTop: height / 100,
    paddingBottom: height / 30,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  category: {
    fontFamily: font.primary_mont,
    fontSize: 13
  },
  categoryButtonContainer: {
    height: 25,
    flex: 0.5
  },
  categoryButton: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderColor: color.maroon
  },
  showContainer: {
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 8,
    alignSelf: "flex-end"
  },
  showOnMap: {
    fontFamily: font.primary_mont_medium,
    fontSize: 14
  }
});

const mapStateToProps = ({
  location: { latitude, longitude, permissionGiven },
  user
}) => ({
  latitude,
  longitude,
  permissionGiven,
  user
});

const mapActionsToProps = {
  setLocation
};

Dashboard.propTypes = {
  setLocation: PropTypes.func.isRequired,
  latitude: PropTypes.number.isRequired,
  longitude: PropTypes.number.isRequired,
  permissionGiven: PropTypes.bool.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    countryCode: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired
  }).isRequired
};

export default connect(mapStateToProps, mapActionsToProps)(Dashboard);
