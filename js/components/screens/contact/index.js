import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    View,
    StyleSheet,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Text,
    ActivityIndicator,
    KeyboardAvoidingView,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';
import { color, font } from '../../theme';
import { Utils, Snackbar } from '../../widgets';
import { HamContainer } from '../../common';
import { QueryAPI, Constants } from '../../../api';
import DialogEvent from '../../../events/dialogEvent';

const { width } = Dimensions.get('window');

class Contact extends Component {
    constructor(props) {
        super(props);
        const {
            name, email, countryCode, phone
        } = this.props;
        this.state = {
            name,
            email,
            phone: countryCode + phone,
            query: '',
            sending: false
        };
        this.onSubmitBind = this.onSubmit.bind(this);
    }

    onChangeValue(type, value) {
        const newState = {};
        newState[type] = value;
        this.setState(newState);
    }

    async onSubmit() {
        try {
            const {
                name, phone, email, query
            } = this.state;
            if (!name.trim()) {
                Utils.shakeAndDisplayToast(this.nameContainer, 'Name cannot be empty.');
                return;
            }
            if (!phone.trim()) {
                Utils.shakeAndDisplayToast(this.phoneContainer, 'Phone number cannot be empty.');
                return;
            }
            if (!email.trim()) {
                Utils.shakeAndDisplayToast(this.emailContainer, 'Email cannot be empty.');
                return;
            }
            if (!query.trim()) {
                Utils.shakeAndDisplayToast(this.queryContainer, 'Query message cannot be empty.');
                return;
            }
            this.setState({
                sending: true
            });
            await QueryAPI.submitQuery(this.props.id, name, phone, email, query);
            this.setState({
                sending: false,
                query: ''
            });
            DialogEvent.alert('Success!', 'Query successfully submitted! We have your query and will get back to you as fast as we can.');
        } catch (err) {
            this.setState({
                sending: false
            });
            if (err === Constants.INVALID_EMAIL) {
                Utils.shakeAndDisplayToast(this.emailContainer, 'Invalid email given. Please try again with the correct one.');
                return;
            }
            if (err === Constants.INTERNAL_SERVER_ERROR) {
                Snackbar.displayInternalServer();
                return;
            }
            Snackbar.displayNetworkError();
        }
    }

    render() {
        const {
            logo,
            body,
            form,
            smallInputContainer,
            smallInput,
            bigInputContainer,
            bigInput,
            submitBtn,
            submit
        } = styles;
        return (
            <HamContainer
                title="Contact"
                navigate={this.props.navigation.navigate}
            >
                <KeyboardAvoidingView
                    behavior="position"
                    contentContainerStyle={{ flex: 1 }}
                    style={{ flex: 1 }}
                >
                    <View style={logo} >
                        <Image style={{ height: width / 4, width: width / 4 }} source={require('../../../../assets/icons/logo_colored.png')} />
                    </View>
                    <View style={{ flex: 0.3 }} />
                    <View
                        style={body}
                    >
                        <View style={form} >
                            <Animatable.View
                                ref={component => { this.nameContainer = component; }}
                                style={smallInputContainer}
                            >
                                <TextInput
                                    ref={component => { this.nameInput = component; }}
                                    style={smallInput}
                                    placeholderColor={color.mediumGrey}
                                    returnKeyType="next"
                                    placeholder="Name"
                                    value={this.state.name}
                                    underlineColorAndroid="transparent"
                                    onChangeText={value => this.onChangeValue('name', value)}
                                    onSubmitEditing={() => this.phoneInput.focus()}
                                />
                            </Animatable.View>
                            <Animatable.View
                                ref={component => { this.phoneContainer = component; }}
                                style={smallInputContainer}
                            >
                                <TextInput
                                    ref={component => { this.phoneInput = component; }}
                                    style={smallInput}
                                    keyboardType="phone-pad"
                                    returnKeyType="next"
                                    placeholderColor={color.mediumGrey}
                                    placeholder="Phone Number"
                                    value={this.state.phone}
                                    underlineColorAndroid="transparent"
                                    onChangeText={value => this.onChangeValue('phone', value)}
                                    onSubmitEditing={() => this.emailInput.focus()}
                                />
                            </Animatable.View>
                            <Animatable.View
                                ref={component => { this.emailContainer = component; }}
                                style={smallInputContainer}
                            >
                                <TextInput
                                    ref={component => { this.emailInput = component; }}
                                    style={smallInput}
                                    keyboardType="email-address"
                                    returnKeyType="next"
                                    placeholderColor={color.mediumGrey}
                                    placeholder="Email Address"
                                    value={this.state.email}
                                    underlineColorAndroid="transparent"
                                    onChangeText={value => this.onChangeValue('email', value)}
                                    onSubmitEditing={() => this.queryInput.focus()}
                                />
                            </Animatable.View>
                            <Animatable.View
                                ref={component => { this.queryContainer = component; }}
                                style={bigInputContainer}
                            >
                                <TextInput
                                    ref={component => { this.queryInput = component; }}
                                    style={bigInput}
                                    returnKeyType="default"
                                    placeholderColor={color.mediumGrey}
                                    placeholder="Query (Max 100 words)"
                                    value={this.state.query}
                                    underlineColorAndroid="transparent"
                                    multiline
                                    maxLength={100}
                                    numberOfLines={3}
                                    onChangeText={value => this.onChangeValue('query', value)}
                                />
                            </Animatable.View>
                            <TouchableOpacity
                                style={submitBtn}
                                onPress={this.onSubmitBind}
                                disabled={this.state.sending}
                            >
                                {!this.state.sending ?
                                    <Text style={submit}>Submit</Text>
                                    : <ActivityIndicator color="black" />
                                }

                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.35 }} />
                    </View>
                </KeyboardAvoidingView>
            </HamContainer>
        );
    }
}

Contact.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func
    }).isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    countryCode: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired
};

const mapStateToProps = ({ user }) => ({
    name: user.name,
    email: user.email,
    countryCode: user.countryCode,
    phone: user.phone,
    id: user.id
});

const styles = StyleSheet.create({
    logo: {
        flex: 1.3,
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        flex: 4,
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    form: {
        marginLeft: width / 15,
        marginRight: width / 15,
        backgroundColor: color.mediumGrey,
        borderRadius: width / 15,
        padding: width / 29,
        flex: 1,
        borderColor: color.grey,
        borderWidth: 0.5,
        paddingBottom: 65
    },
    smallInputContainer: {
        backgroundColor: 'white',
        borderRadius: width / 20,
        height: 40,
        marginBottom: width / 35,
        paddingLeft: 15,
        paddingRight: 15,
        borderColor: color.grey,
        borderWidth: 0.5
    },
    smallInput: {
        fontFamily: font.primary_mont,
        fontSize: 12,
        flex: 1
    },
    bigInputContainer: {
        backgroundColor: 'white',
        borderRadius: width / 20,
        height: 80,
        marginBottom: width / 35,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingTop: 15,
        borderColor: color.grey,
        borderWidth: 0.5
    },
    bigInput: {
        fontFamily: font.primary_mont,
        fontSize: 12,
        flex: 1,
        textAlign: 'left'
    },
    submitBtn: {
        width: width / 4,
        backgroundColor: color.green,
        borderColor: color.grey,
        borderWidth: 0.5,
        borderRadius: width / 10,
        height: 45,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: width / 45
    },
    submit: {
        fontFamily: font.primary_mont_medium,
        fontSize: 16
    }
});

export default connect(mapStateToProps, null)(Contact);
