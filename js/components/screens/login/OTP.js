import React from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Animated
} from "react-native";
import { IconInput } from "../../common";
import { font } from "../../theme";
import { MeegoLoginAPI, Constants } from "../../../api";
import { Snackbar, Utils } from "../../widgets";
import Logger from "../../../utils/logging";

class OTP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
    this.otp = "";
    this.otpProceedBind = this.otpProceed.bind(this);
  }

  componentDidMount() {
    if (this.props.clearObservable) {
      this.clearDisposable = this.props.clearObservable.subscribe(() => {
        this.otpRef.setText("");
      });
    }
  }

  componentWillUnmount() {
    if (this.props.clearObservable) {
      this.clearDisposable.unsubscribe();
    }
  }
  async otpProceed() {
    if (this.state.loading) {
      return;
    }
    if (!this.otp) {
      Utils.shakeAndDisplayToast(
        this.otpView,
        "OTP cannot be empty. Please try again."
      );
      return;
    }
    this.setState({
      loading: true
    });
    try {
      await MeegoLoginAPI.OTP.verifyOTP(this.otp.trim());
      this.setState({
        loading: false
      });
      this.props.onVerify();
    } catch (err) {
      this.setState({
        loading: false
      });
      switch (err) {
        case Constants.OTP_INVALID:
          Utils.shakeAndDisplayToast(
            this.otpView,
            "Entered OTP is invalid. Please try again."
          );
          break;
        case Constants.OTP_EXPIRED:
          Utils.shakeAndDisplayToast(
            this.otpView,
            "Entered OTP has expired. Please go back and try again."
          );
          break;
        case Constants.MOBILE_NOT_FOUND:
          Snackbar.displayToast(
            "Entered phone number not found. Please change the number and try again."
          );
          break;
        case Constants.INVALID_MOBILE:
          Snackbar.displayToast(
            "Entered phone number is invalid found. Please change the number and try again."
          );
          break;
        case Constants.INTERNAL_SERVER_ERROR:
          Snackbar.displayInternalServer();
          break;
        default:
          Snackbar.displayNetworkError();
      }
      Logger.warn(
        "Couldn't validate otp",
        "onSubmit:catch",
        { otp: this.otp.trim() },
        err
      );
    }
  }

  render() {
    const { header, loginButton, buttonText } = styles;
    return (
      <Animated.View
        style={[
          {
            position: "absolute",
            top: 0,
            bottom: 0,
            right: 0,
            left: 0
          },
          this.props.transform
        ]}
      >
        <View style={header} />
        <View
          style={{
            flex: 0.45
          }}
        >
          <IconInput
            ref={component => {
              this.otpRef = component;
            }}
            viewRef={component => {
              this.otpView = component;
            }}
            image={
              <Image
                source={require("../../../../assets/icons/unlocked_white.png")}
                resizeMode="contain"
                style={{ flex: 1, width: 22 }}
              />
            }
            placeholder="OTP"
            inputType="phone-pad"
            onChangeText={value => {
              this.otp = value;
            }}
          />
        </View>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: 0.02 }} />
          <View style={{ flex: 1, flexDirection: "column" }}>
            <View style={{ flex: 3.2 }} />
            <TouchableOpacity style={loginButton} onPress={this.otpProceedBind}>
              {this.state.loading ? (
                <ActivityIndicator color="black" />
              ) : (
                <Text style={buttonText}>Verify</Text>
              )}
            </TouchableOpacity>
            <View style={{ flex: 1 }} />
          </View>
          <View style={{ flex: 0.02 }} />
        </View>
      </Animated.View>
    );
  }
}

OTP.propTypes = {
  onVerify: PropTypes.func.isRequired,
  transform: PropTypes.shape({
    transform: PropTypes.array
  }),
  clearObservable: PropTypes.any
};

OTP.defaultProps = {
  transform: { transform: [] },
  clearObservable: null
};

const styles = StyleSheet.create({
  header: {
    flex: 1.5,
    flexDirection: "column",
    alignItems: "center"
  },
  loginButton: {
    borderRadius: 1,
    flex: Platform.OS === "android" ? 1.5 : 1.2,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 8.5,
    marginLeft: 8.5
  },
  buttonText: {
    fontFamily: font.primary,
    fontSize: 20
  }
});

export default OTP;
