import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  View,
  ActivityIndicator,
  Text,
  ScrollView,
  Dimensions,
  TextInput,
  TouchableOpacity
} from "react-native";
import { HamContainer, NearbyPlace } from "../../common";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import { color, font } from "../../theme";
import { BASE_PATH, NEW_BASE_PATH } from "../../../utils/constants";
const { height, width } = Dimensions.get("window");

class ListPlaces extends Component {
  static navigationOptions = {
    drawerLabel: () => null
  };
  constructor(props) {
    super(props);
    let {
      nearbyPlaces,
      nearbyChoice,
      nearbyPlacesLoading,
      selectedLocation
    } = this.props.navigation.state.params;
    this.state = {
      places: nearbyPlaces[nearbyChoice], // we are setting the initial state with the data you import
      locationName: selectedLocation
    };
  }

  onLearnMore = place => {
    this.props.navigation.navigate("ExploreNearby", { place });
  };

  filterSearch(text) {
    let { nearbyPlaces, nearbyChoice } = this.props.navigation.state.params;
    const newData = nearbyPlaces[nearbyChoice].filter(item => {
      const itemData = item.name.toLowerCase();
      const textData = text.toLowerCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      text: text,
      places: newData // after filter we are setting users to new array
    });
  }

  render() {
    let { nearbyPlacesLoading } = this.props.navigation.state.params;
    let { goBack } = this.props.navigation;
    //console.log('place: ', this.props.navigation.state.params);
    if (nearbyPlacesLoading) {
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator color={color.maroon} />
        </View>
      );
    }
    // rather than mapping users loaded from data file we are using state value
    return (
      <HamContainer
        title=""
        navigate={this.props.navigation.navigate}
        switchView
        iconDisplay="image"
        iconColor
      >
        <View
          style={{
            margin: 10,
            marginBottom: 0,
            borderTopWidth: 1,
            borderColor: color.lightGrey,
            padding: 10,
            paddingLeft: 5,
            paddingRight: 5,
            paddingBottom: 0
          }}
        >
          <Text
            style={{
              color: color.darkgrey,
              fontFamily: font.primary_mont_regular
            }}
          >
            YOUR LOCATION
          </Text>
          <TouchableOpacity
            style={{
              marginTop: 3,
              flexDirection: "row",
              justifyContent: "space-between",
              flexWrap: "wrap"
            }}
            onPress={() => {
              this.props.navigation.navigate("Dashboard");
            }}
          >
            <View style={{ flex: 4, flexDirection: "row" }}>
              <MaterialIcons name="location-on" size={25} color="#099E44" />
              <Text
                numberOfLines={3}
                style={{
                  fontSize: 20,
                  fontFamily: font.primary_mont_semi_bold
                }}
              >
                {this.state.locationName.length > 2
                  ? this.state.locationName
                  : "No Location Selected"}
              </Text>
            </View>
            <Text
              style={{
                flex: 1,
                fontFamily: font.primary_mont_medium,
                color: "#099E44",
                alignItems: "center",
                marginTop: 5,
                letterSpacing: 2,
                marginLeft: 15
              }}
            >
              CHANGE
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            backgroundColor: color.lightGrey,
            margin: 10,
            padding: 10,
            borderRadius: 4,
            flexDirection: "row"
          }}
        >
          <Ionicons
            name="ios-search"
            size={20}
            color={color.darkgrey}
            style={{ alignSelf: "center" }}
          />
          <TextInput
            style={{
              flex: 1,
              paddingLeft: width / 25,
              paddingRight: width / 25,
              paddingTop: 0,
              paddingBottom: 0,
              fontFamily: font.primary_mont_medium,
              fontSize: 16
            }}
            onChangeText={text => this.filterSearch(text)}
            value={this.state.text}
            underlineColorAndroid="transparent"
            placeholder="Search for restaurants"
            placeholderTextColor={color.darkgrey}
            blurOnSubmit
            ref={component => (this._root = component)}
          />
        </View>
        <View
          style={{
            flex: 1,
            alignSelf: "auto",
            flexDirection: "column",
            backgroundColor: color.lightGrey
          }}
        >
          <ScrollView>
            {this.state.places.length ? (
              this.state.places.map((place, index) => (
                <NearbyPlace
                  key={place.id}
                  name={place.name}
                  distance={place.distance}
                  description={place.description}
                  state={place.state}
                  imagePath={place.image}
                  onExplore={() => this.onLearnMore(place)}
                />
              ))
            ) : (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text style={{ fontFamily: font.primary_mont }}>
                  No places found.
                </Text>
              </View>
            )}
          </ScrollView>
        </View>
      </HamContainer>
    );
  }
}

export default ListPlaces;
