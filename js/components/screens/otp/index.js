import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  BackHandler,
  KeyboardAvoidingView
} from "react-native";
import * as Animatable from "react-native-animatable";
import PropTypes from "prop-types";
import { Font } from "expo";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import { setUser } from "../../../actions";
import { IconInput, MeegoOTPBackground, MeegoBackground } from "../../common";
import { font } from "../../theme";
import { MeegoLoginAPI, Constants } from "../../../api";
import { Snackbar, Utils } from "../../widgets";
import { FormLabel, FormInput, Button } from "react-native-elements";
import { TextInput } from "react-native";

import Logger from "../../../utils/logging";
import Events from "../../../utils/events";

class OTP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      showOTP: false,
      OTPVerified: false,
      otpArray: []
    };

    this.inputs = {};

    this.countryCode = "" || "+91";
    this.phone = "";
    this.onProceedBind = this.onProceed.bind(this);
    this.onProceedVerifiedOK = this.onProceedVerifiedOK.bind(this);
    this.onResendBind = this.onResend.bind(this);
    this.onBackPressBind = this.onBackPress.bind(this);
    this.otpMergeBind = this.otpMerge.bind(this);
    const {
      goBack,
      dispatch,
      state: { params }
    } = props.navigation;
    this.goBack = goBack;
    this.navigationDispatch = dispatch;
    this.navigationParams = params;
    this.hardwareBackPress = () => {
      if (this.state.showOTP) {
        this.fadeInPhone();
        return true;
      }
      return false;
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.hardwareBackPress);

    Font.loadAsync({
      "intro-bold": require("../../../../assets/fonts/Intro-Bold.otf"),
      "intro-regular-alt": require("../../../../assets/fonts/Intro-Regular-Alt.otf")
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.hardwareBackPress
    );
  }

  // Let's send the OTP here.
  async onSend() {
    if (!this.phone) {
      Utils.shakeAndDisplayToast(
        this.phoneView,
        "Phone cannot be empty. Please try again."
      );
      return;
    }
    if (this.phone.length < 10) {
      Utils.shakeAndDisplayToast(
        this.phoneView,
        "Phone number not valid. Please try again."
      );
      return;
    }
    this.setState({
      loading: true
    });
    try {
      Snackbar.displayToast("OTP has been sent to your registered number");
      await MeegoLoginAPI.OTP.sendOTP(
        this.countryCode.trim(),
        this.phone.trim()
      );
      this.setState({
        loading: false
      });
      this.fadeInOTP();
    } catch (err) {
      Logger.warn(
        "Couldn't send otp.",
        "onSend:catch",
        {
          countryCode: this.countryCode.trim(),
          phone: this.phone.trim()
        },
        err
      );
      this.setState({
        loading: false
      });
      if (err === Constants.INTERNAL_SERVER_ERROR) {
        Snackbar.displayInternalServer();
        return;
      }
      Snackbar.displayNetworkError();
    }
  }

  // Let's verfiy the OTP here.
  async onVerify() {
    let otpTxt = this.state.otpArray.join("");
    if (!otpTxt) {
      Utils.shakeAndDisplayToast(
        this.otpView,
        "OTP cannot be empty. Please try again."
      );
      return;
    }
    this.setState({
      loading: true
    });
    try {
      await MeegoLoginAPI.OTP.verifyOTP(otpTxt.trim());
      // Register the user after the OTP has been verified.
      const { name, email, password } = this.navigationParams;
      const newUser = await MeegoLoginAPI.registerUser(
        email,
        password,
        this.countryCode,
        this.phone,
        name
      );

      Events.setIdentity(email, {
        name
      });
      Events.track("User registered", { email });

      Logger.setUserContext(newUser.id, email, name);

      this.setState({
        loading: false,
        OTPVerified: true
      });

      this.props.setUser(newUser);

      // this.pushToDashboard();
    } catch (err) {
      this.setState({
        loading: false
      });
      switch (err) {
        case Constants.OTP_INVALID:
          Utils.shakeAndDisplayToast(
            this.otpView,
            "Entered OTP is invalid. Please try again."
          );
          break;
        case Constants.OTP_EXPIRED:
          Utils.shakeAndDisplayToast(
            this.otpView,
            "Entered OTP has expired. Please go back and try again."
          );
          break;
        case Constants.MOBILE_NOT_FOUND:
          Snackbar.displayToast(
            "Entered phone number not found. Please change the number and try again."
          );
          break;
        case Constants.INVALID_MOBILE:
          Snackbar.displayToast(
            "Entered phone number is invalid found. Please change the number and try again."
          );
          break;
        case Constants.INTERNAL_SERVER_ERROR:
          Snackbar.displayInternalServer();
          break;
        default:
          Snackbar.displayNetworkError();
      }
      Logger.warn(
        "Couldn't validate otp or register user.",
        "onSubmit:catch",
        { otp: this.otp.trim() },
        err
      );
    }
  }

  async onResend() {
    try {
      await MeegoLoginAPI.OTP.resendOTP();
      Snackbar.displayToast("OTP has been successfully resent.");
    } catch (err) {
      Logger.warn(
        "Couldn't resend otp.",
        "onResend:catch",
        {
          countryCode: this.countryCode.trim(),
          phone: this.phone.trim()
        },
        err
      );
      this.setState({
        loading: false
      });
      if (err === Constants.INTERNAL_SERVER_ERROR) {
        Snackbar.displayInternalServer();
        return;
      }
      Snackbar.displayNetworkError();
    }
  }

  onBackPress() {
    if (this.state.showOTP) {
      this.fadeInPhone();
      return;
    }
    this.goBack();
  }

  onProceed() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.showOTP) {
      this.onSend();
      return;
    }
    this.onVerify();
  }
  onProceedVerifiedOK() {
    if (this.state.loading) {
      return;
    }
    this.pushToDashboard();
  }

  pushToDashboard() {
    const action = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Dashboard" })]
    });
    this.navigationDispatch(action);
  }

  fadeInOTP() {
    this.otp = "";
    this.setState({
      showOTP: true
    });
    // this.phoneRef.fadeOut(300).then(() => {
    //     this.setState({
    //         showOTP: true
    //     }, () => {
    //         this.otpView.fadeIn(350);
    //     });
    // });
  }

  fadeInPhone() {
    this.phone = "";
    // this.otpView.fadeOut(300).then(() => {
    this.setState({
      showOTP: false
      // }, () => {
      //     this.phoneRef.fadeIn(350);
      // });
    });
  }

  otpMerge(index, value) {
    let otp = this.state.otpArray.slice();
    otp[index] = value;
    this.setState({ otpArray: otp });

    if (index < 5) {
      this.inputs[index + 1].focus();
    }
  }

  render() {
    const {
      header,
      loginButton,
      buttonText,
      backButton,
      logoStyle,
      skipButton,
      headerImageContainer,
      footer,
      resendButton,
      resendText
    } = styles;
    if (this.state.OTPVerified) {
      return (
        <MeegoBackground>
          <View
            style={{
              flex: 1,
              justifyContent: "flex-end",
              alignItems: "center"
            }}
          >
            <Text
              style={{ textAlign: "center", fontFamily: "intro-regular-alt" }}
            >
              <Text style={{ fontSize: 30, paddingRight: 5 }}>{"Hello "}</Text>
              <Text style={{ fontSize: 24 }}>
                {this.navigationParams.name},{" "}
              </Text>
            </Text>
            <Text
              style={{
                fontSize: 24,
                textAlign: "center",
                paddingLeft: 10,
                paddingRight: 10,
                fontFamily: "intro-regular-alt"
              }}
            >
              {"\n"}Thank you for registering with Bingle
            </Text>
          </View>
          <View style={{ flex: 1, marginTop: 30, alignItems: "center" }}>
            <Button
              title="OK"
              onPress={this.onProceedVerifiedOK}
              buttonStyle={{
                backgroundColor: "#007EDD",
                borderWidth: 0,
                borderRadius: 30,
                height: 45,
                width: 200
              }}
              containerStyle={{ marginVertical: 10, height: 45, width: 200 }}
              textStyle={{ fontSize: 24, fontFamily: "intro-regular-alt" }}
            />
          </View>
        </MeegoBackground>
      );
    } else if (this.state.showOTP) {
      return (
        <MeegoBackground>
          <View style={header}>
            {/* <TouchableOpacity
                                style={skipButton}
                                onPress={this.onBackPressBind}
                            >
                                <Image resizeMode="contain" style={backButton} source={require('../../../../assets/icons/left_arrow.png')} />
                            </TouchableOpacity> */}
            <View style={{ flex: 1 }} />
            <View style={[headerImageContainer, { marginTop: 70 }]}>
              <View style={{ flex: 0.94 }} />
              <Text
                style={{
                  textDecorationLine: "underline",
                  textAlign: "center",
                  fontSize: 32,
                  fontFamily: "intro-regular-alt",
                  color: "white"
                }}
              >
                WELCOME
              </Text>
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 18,
                  fontFamily: "intro-regular-alt",
                  marginTop: 10
                }}
              >
                Shiven
              </Text>
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 20,
                  fontFamily: "intro-regular-alt",
                  marginTop: 10
                }}
              >
                Enter Your OTP
              </Text>
              <View style={{ flex: 0.5 }} />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "center",
              marginTop: 20
            }}
          >
            <View
              style={{
                flex: 0.7,
                flexDirection: "row",
                justifyContent: "space-around"
              }}
            >
              <TextInput
                underlineColorAndroid={"transparent"}
                ref={input => (this.inputs["0"] = input)}
                placeholderTextColor="black"
                maxLength={1}
                keyboardType="phone-pad"
                style={styles.otpInput}
                onChangeText={value => this.otpMergeBind(0, value)}
              />
              <TextInput
                underlineColorAndroid={"transparent"}
                ref={input => (this.inputs["1"] = input)}
                placeholderTextColor="black"
                maxLength={1}
                keyboardType="phone-pad"
                style={styles.otpInput}
                onChangeText={value => this.otpMergeBind(1, value)}
              />
              <TextInput
                underlineColorAndroid={"transparent"}
                ref={input => (this.inputs["2"] = input)}
                placeholderTextColor="black"
                maxLength={1}
                keyboardType="phone-pad"
                style={styles.otpInput}
                onChangeText={value => this.otpMergeBind(2, value)}
              />
              <TextInput
                underlineColorAndroid={"transparent"}
                ref={input => (this.inputs["3"] = input)}
                placeholderTextColor="black"
                maxLength={1}
                keyboardType="phone-pad"
                style={styles.otpInput}
                onChangeText={value => this.otpMergeBind(3, value)}
              />
              <TextInput
                underlineColorAndroid={"transparent"}
                ref={input => (this.inputs["4"] = input)}
                placeholderTextColor="black"
                maxLength={1}
                keyboardType="phone-pad"
                style={styles.otpInput}
                onChangeText={value => this.otpMergeBind(4, value)}
              />
              <TextInput
                underlineColorAndroid={"transparent"}
                ref={input => (this.inputs["5"] = input)}
                placeholderTextColor="black"
                maxLength={1}
                keyboardType="phone-pad"
                style={styles.otpInput}
                onChangeText={value => this.otpMergeBind(5, value)}
              />
            </View>
          </View>
          <View style={{ flex: 1, width: "80%", alignSelf: "center" }}>
            <Button
              title="VERIFY"
              onPress={this.onProceedBind}
              buttonStyle={{
                backgroundColor: "#007EDD",
                borderWidth: 0,
                borderRadius: 30,
                height: 45
              }}
              containerStyle={{ marginVertical: 10, height: 45, width: 310 }}
              textStyle={{ fontSize: 24, fontFamily: "intro-regular-alt" }}
            />
          </View>

          <View style={{ flex: 1.48, flexDirection: "row" }}>
            <View style={{ flex: 0.02 }} />
            <View style={{ flex: 1, flexDirection: "column" }}>
              <View style={{ flex: 3.2 }} />
              <View style={footer}>
                {this.state.showOTP ? (
                  <TouchableOpacity
                    onPress={this.onResendBind}
                    style={resendButton}
                  >
                    <Text style={resendText}>Request a Call</Text>
                  </TouchableOpacity>
                ) : null}
              </View>
            </View>
            <View style={{ flex: 0.02 }} />
          </View>
        </MeegoBackground>
      );
    } else {
      return (
        <MeegoOTPBackground>
          <View style={header}>
            <TouchableOpacity style={skipButton} onPress={this.onBackPressBind}>
              <Image
                resizeMode="contain"
                style={backButton}
                source={require("../../../../assets/icons/left_arrow.png")}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1
            }}
          />
          <KeyboardAvoidingView
            behavior="position"
            style={{
              flex: 2,
              padding: 30,
              justifyContent: "flex-end"
            }}
          >
            <View
              style={{ flex: 1, flexDirection: "column", paddingTop: 100 }}
            />
            <FormInput
              placeholder="+91"
              placeholderTextColor="black"
              keyboardType="phone-pad"
              onChangeText={value => {
                this.countryCode = value || "+91";
              }}
              inputStyle={{
                fontSize: 20,
                fontFamily: "intro-regular-alt",
                paddingBottom: 15,
                paddingRight: 5,
                paddingLeft: 5
              }}
            />

            <View style={{ marginBottom: 20 }} />

            <FormInput
              containerRef={component => {
                this.phoneView = component;
              }}
              textInputRef={component => {
                this.phoneInput = component;
              }}
              placeholder="Your phone number"
              placeholderTextColor="#B4B4B4"
              keyboardType="phone-pad"
              onChangeText={value => {
                this.phone = value;
              }}
              inputStyle={{
                fontSize: 20,
                fontFamily: "intro-regular-alt",
                paddingBottom: 15,
                paddingRight: 5,
                paddingLeft: 5
              }}
            />
          </KeyboardAvoidingView>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 0.02 }} />
            <View style={{ flex: 1, flexDirection: "column", padding: 30 }}>
              <View style={{ flex: 3.2 }} />
              <Button
                title={this.state.showOTP ? "COMPLETE" : "SUBMIT"}
                onPress={this.onProceedBind}
                buttonStyle={{
                  backgroundColor: "#00E3FE",
                  borderWidth: 2,
                  borderColor: "white",
                  borderRadius: 30,
                  height: 55
                }}
                containerStyle={{ marginVertical: 10, height: 55, width: 150 }}
                textStyle={{ fontSize: 24, fontFamily: "intro-regular-alt" }}
              />

              <View style={footer}>
                {this.state.showOTP ? (
                  <TouchableOpacity
                    onPress={this.onResendBind}
                    style={resendButton}
                  >
                    <Text style={resendText}>Complete using call</Text>
                  </TouchableOpacity>
                ) : null}
              </View>
            </View>
            <View style={{ flex: 0.02 }} />
          </View>
          {/* </KeyboardAvoidingView> */}
        </MeegoOTPBackground>
      );
    }
  }
}

OTP.propTypes = {
  setUser: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired
};

const styles = StyleSheet.create({
  header: {
    flex: 1.5,
    flexDirection: "column",
    alignItems: "center"
  },
  loginButton: {
    borderRadius: 1,
    flex: Platform.OS === "android" ? 1.5 : 1.2,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 8.5,
    marginLeft: 8.5
  },
  buttonText: {
    fontFamily: font.primary,
    fontSize: 20
  },
  backButton: {
    width: 30,
    height: 30
  },
  logoStyle: {
    flex: 1,
    width: 130
  },
  skipButton: {
    flex: 0.4,
    backgroundColor: "#00000000",
    position: "absolute",
    paddingLeft: 20.5,
    paddingTop: 30,
    left: 0,
    top: 20
  },
  headerImageContainer: {
    flex: 1.6,
    flexDirection: "column"
  },
  footer: {
    flex: 1,
    backgroundColor: "#00000000",
    marginRight: 8.5,
    marginLeft: 8.5,
    alignItems: "flex-end"
  },
  resendButton: {
    flex: 1,
    justifyContent: "center"
  },
  resendText: {
    fontFamily: font.primary,
    color: "white"
  },
  otpInput: {
    width: 40,
    height: 45,
    borderColor: "white",
    padding: 3,
    borderBottomWidth: 0,
    backgroundColor: "white",
    borderRadius: 10,
    marginTop: 30,
    textAlign: "center"
  }
});

export default connect(null, { setUser })(OTP);
