import React from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    Image,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ActivityIndicator,
    Platform
} from 'react-native';
import { ImagePicker } from 'expo';
import { MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import RadioForm from 'react-native-simple-radio-button';
import { HamContainer } from '../../common';
import { color, font } from '../../theme';
import { Snackbar } from '../../widgets';
import { ProfileAPI, Constants } from '../../../api';
import { alterUser, setAvatar } from '../../../actions';
import { LoadingEvent, DialogEvent } from '../../../events';
import { BASE_PATH,NEW_BASE_PATH } from '../../../utils/constants';

const { height, width } = Dimensions.get('window');
const radioProps = [
    { label: 'Male', value: 'male' },
    { label: 'Female', value: 'female' }
];

class Profile extends React.Component {
    constructor(props) {
        super(props);
        const {
            name, phone, countryCode, email, age, gender
        } = props.user;
        this.state = {
            editable: true,
            name: name || 'Jivitesh Sharma',
            email: email || 'jivitesh@gmail.com',
            phone: phone ? `${countryCode} ${phone}` : '+91 9876543210',
            language: 'English',
            country: 'India',
            savingDetails: false,
            paytm: `Paytm Wallet | +91 ${props.payTMNumber}`,
            password: 'aasdsa',
            net: 'Net Banking | XXXXXX5960',
            gender,
            age: age ? `${age}` : ''
        };
        this.saveDetailsBind = this.saveDetails.bind(this);
        this.changeAvatarBind = this.changeAvatar.bind(this);
    }

    async changeAvatar() {
        const chooseCamera = await DialogEvent.alert('Use camera?', 'Use the camera to take a new picture of you or select an old one from your gallery?', 'Open Camera', true, 'Open Gallery');
        const options = {
            base64: true,
            allowsEditing: true
        };
        const { cancelled, base64 } = chooseCamera
            ? await ImagePicker.launchCameraAsync(options)
            : await ImagePicker.launchImageLibraryAsync(options);
        if (!cancelled) {
            try {
                LoadingEvent.show();
                const { message } = await ProfileAPI.updateProfilePicture(this.props.user.id, `data:image/jpeg;base64,${base64}`);
                Snackbar.displayToast('Avatar succesfully changed!');
                this.props.setAvatar(message);
                LoadingEvent.cancel();
            } catch (err) {
                LoadingEvent.cancel();
                if (err === Constants.INTERNAL_SERVER_ERROR) {
                    Snackbar.displayInternalServer();
                    return;
                }
                Snackbar.displayNetworkError();
            }
        }
    }

    async saveDetails() {
        let {
            name,
            email,
            age,
            gender
        } = this.state;
        name = name.trim();
        email = email.trim();
        age -= 0;
        if (!name) {
            Snackbar.displayToast('Name cannot be empty. Please try again.');
            return;
        }
        if (!email) {
            Snackbar.displayToast('Email cannot be empty. Please try again.');
            return;
        }
        this.setState({
            savingDetails: true
        });
        try {
            const user = {
                name, email
            };
            if (age) {
                user.age = age;
            }
            if (gender && gender !== 'na') {
                user.gender = gender;
            }
            await ProfileAPI.updateProfile(this.props.user.id, user);
            Snackbar.displayToast('Profile details successfully updated!');
            this.setState({
                savingDetails: false
            });
            this.props.alterUser(user);
        } catch (err) {
            console.log(err);
            this.setState({
                savingDetails: false
            });
            if (err === Constants.INTERNAL_SERVER_ERROR) {
                Snackbar.displayInternalServer();
                return;
            }
            Snackbar.displayNetworkError();
        }
    }

    getGenderIndex() {
        switch (this.state.gender) {
        case 'male':
            return 0;
        case 'female':
            return 1;
        default:
            return -1;
        }
    }

    render() {
        return (
            <HamContainer
                title="Edit Profile"
                navigate={this.props.navigation.navigate}
            >
                <View style={{
                    flexDirection: 'column', paddingVertical: width / 35, flex: 1, backgroundColor: 'white'
                }}
                >
                    <TouchableWithoutFeedback onPress={this.changeAvatarBind}>
                        <View style={{ alignSelf: 'center' }}>
                            <Image
                                style={{
                                    alignSelf: 'center', borderRadius: 55, height: 110, width: 110
                                }}
                                source={this.props.user.avatar ? { uri: NEW_BASE_PATH + this.props.user.avatar } : require('../../../../assets/icons/avatar.png')}
                            />
                            <View
                                style={{
                                    borderRadius: height / 15,
                                    height: height / 25,
                                    width: height / 25,
                                    position: 'absolute',
                                    right: 0,
                                    bottom: 5,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: 'white',
                                    borderWidth: 0.5,
                                    borderColor: color.grey
                                }}
                            >
                                <MaterialIcons
                                    name="mode-edit"
                                    size={height / 33}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{ justifyContent: 'space-around', flex: 1, paddingVertical: width / 20 }} >
                        <View style={styles.textInputContainer} >
                            <Text style={styles.text} >Full Name </Text>
                            <TextInput
                                value={this.state.name}
                                style={styles.textInput}
                                underlineColorAndroid="transparent"
                                editable={this.state.editable}
                                ref={(component) => this._root = component}
                                onChangeText={(text) => { this.setState({ name: text }); }}
                            />
                        </View>
                        <View style={styles.textInputContainer} >
                            <View style={{ flexDirection: 'row' }}><Text style={styles.text} >Email</Text></View>
                            <TextInput
                                value={this.state.email}
                                style={styles.textInput}
                                underlineColorAndroid="transparent"
                                editable={this.state.editable}
                                ref={(component) => this._root = component}
                                onChangeText={(text) => { this.setState({ email: text }); }}
                            />
                        </View>
                        <View style={[styles.textInputContainer, { opacity: 0.5 }]} >
                            {<View style={{ flexDirection: 'row' }}><Text style={styles.text} >Phone Number | </Text><Text style={[styles.text, { color: '#1BE249' }]}>Verified</Text></View> }
                            <TextInput
                                value={this.state.phone}
                                style={styles.textInput}
                                underlineColorAndroid="transparent"
                                editable={false}
                                ref={(component) => this._root = component}
                                onChangeText={(text) => { this.setState({ phone: text }); }}
                            />
                        </View>
                        <View style={[styles.textInputContainer, { flexDirection: 'column', justifyContent: 'center' }]}>
                            <Text style={[styles.text, { paddingBottom: 12 }]}>
                                Gender
                            </Text>
                            <RadioForm
                                radio_props={radioProps}
                                initial={this.getGenderIndex()}
                                labelHorizontal
                                buttonColor={color.green}
                                selectedButtonColor={color.green}
                                formHorizontal
                                style={{ justifyContent: 'space-between', width: width / 2 }}
                                animation={Platform.OS === 'ios'}
                                onPress={(value) => { this.setState({ gender: value }); }}
                            />
                        </View>
                        <View style={styles.textInputContainer} >
                            <Text style={styles.text} >Age </Text>
                            <TextInput
                                value={this.state.age}
                                style={styles.textInput}
                                placeHolder="Please enter your age"
                                underlineColorAndroid="transparent"
                                editable={this.state.editable}
                                keyboardType="numeric"
                                onChangeText={(text) => { this.setState({ age: text }); }}
                            />
                        </View>
                        {/* <View style={styles.textInputContainer} >
                            <Text style={styles.text} >Country and Language </Text>
                            <View style={styles.languageContainer}>
                                <TextInput
                                    value={this.state.country}
                                    style={[styles.textInput, { minWidth: 45 }]}
                                    underlineColorAndroid="transparent"
                                    editable={false}
                                    ref={(component) => this._root = component}
                                    onChangeText={(text) => { this.setState({ country: text }); }}
                                />
                                <Text
                                    style={{
                                        fontFamily: font.primary_mont,
                                        fontSize: 16,
                                        paddingRight: 5
                                    }}
                                >|
                                </Text>
                                <TextInput
                                    value={this.state.language}
                                    style={styles.textInput}
                                    underlineColorAndroid="transparent"
                                    editable={false}
                                    ref={(component) => this._root = component}
                                    onChangeText={(text) => { this.setState({ language: text }); }}
                                />
                            </View>
                        </View>
                      */}
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Payment Methods')} style={styles.textInputContainer} > */}
                        {/* <View style={{ flexDirection: 'row' }}><Text style={styles.text} >Payment Method | </Text><Text style={[styles.text, { color: '#1BE249' }]}>Verified</Text></View> */}
                        {/* <Text style={styles.textInput}>{this.state.paytm}</Text> */}
                        {/* <Text style={styles.textInput}>{this.state.net}</Text> */}
                        {/* </TouchableOpacity> */}
                        <View style={[styles.textInputContainer, { opacity: 0.5 }]} >
                            <View style={{ flexDirection: 'row' }}><Text style={styles.text} >Password </Text></View>
                            <TextInput
                                value={this.state.password}
                                style={styles.textInput}
                                secureTextEntry
                                underlineColorAndroid="transparent"
                                editable={false}
                                ref={(component) => this._root = component}
                                onChangeText={(text) => { this.setState({ value: text }); }}
                            />
                        </View>
                        <View />
                    </View>
                    <TouchableOpacity
                        disabled={this.state.savingDetails}
                        style={styles.button}
                        onPress={this.saveDetailsBind}
                    >
                        {
                            this.state.savingDetails ?
                                <ActivityIndicator color="black" />
                                : <Text style={[styles.text, styles.number]}>Update Details</Text>
                        }

                    </TouchableOpacity>
                </View>
            </HamContainer>
        );
    }
}

Profile.navigationOptions = {
    drawerLockMode: 'locked-closed'
};

Profile.propTypes = {
    user: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        countryCode: PropTypes.string.isRequired,
        avatar: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired
    }).isRequired,
    navigation: PropTypes.shape({
        goBack: PropTypes.func,
        navigate: PropTypes.func
    }).isRequired,
    alterUser: PropTypes.func.isRequired,
    setAvatar: PropTypes.func.isRequired
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        fontFamily: font.primary_mont_medium,
        fontSize: 11,
        color: color.darkgrey
    },
    textCenter: {
        textAlign: 'center',
        fontFamily: font.primary_mont,

    },
    textInput: {
        minWidth: 150,
        fontFamily: font.primary_mont,
        fontSize: 16
    },
    textInputContainer: {
        flexDirection: 'column',

        paddingLeft: width / 20
    },
    languageContainer: {
        flexDirection: 'row'
    },
    number: {
        fontSize: 15,
        fontFamily: font.primary_mont_medium,
        color: 'black'
    },
    button: {
        backgroundColor: color.green,
        height: 60,
        width: '80%',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        bottom: 10
    }
});

const mapStateToProps = ({ user }) => ({
    user
});

export default connect(mapStateToProps, { alterUser, setAvatar })(Profile);
