import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    StyleSheet,
    Text,
    FlatList,
    TouchableOpacity,
    Dimensions,
    RefreshControl,
    ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import * as _ from 'lodash';
import { color, font } from '../../theme';
import { HamContainer } from '../../common';
import { BookingAPI, Constants } from '../../../api';
import { Snackbar } from '../../widgets';

const { width } = Dimensions.get('window');

class BookingList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bookings: [],
            refreshing: false,
            loading: false
        };
        this.onRefreshBind = this.onRefresh.bind(this);
        this.renderBookingBind = this.renderBooking.bind(this);
        this.getCallHistoryBind = this.getCallHistory.bind(this);
        this.renderEmptyComponentBind = this.renderEmptyComponent.bind(this);
        this.offset = 0;
        this.listEnded = false;
    }

    pushToDetails(booking) {
        this.props.navigation.navigate('BookingDetails', booking);
    }

    onRefresh() {
        this.setState({
            refreshing: true,
            bookings: []
        }, async () => {
            this.offset = 0;
            this.listEnded = false;
            await this.getCallHistory();
            this.setState({
                refreshing: false
            });
        });
    }

    async getCallHistory() {
        if (!this.listEnded) {
            try {
                this.setState({
                    loading: true
                });
                const bookings = await BookingAPI.getCallHistory(this.props.id, this.offset);
                this.setState({
                    bookings: _.concat(this.state.bookings, bookings),
                    loading: false
                });
                this.offset += 1;
                if (!bookings.length) {
                    this.listEnded = true;
                }
            } catch (err) {
                console.log(err);
                if (err === Constants.INTERNAL_SERVER_ERROR) {
                    Snackbar.displayInternalServer();
                    return;
                }
                Snackbar.displayNetworkError();
            }
        }
    }

    renderBooking({ item }) {
        const {
            fare, duration, date, year, time
        } = item;
        const {
            listItem,
            itemStyle,
            innerItem,
            price,
            durationStyle,
            timeStyle,
            dateContainer,
            dateStyle,
            yearStyle
        } = styles;
        return (
            <View style={listItem} >
                <View style={{ flex: 1.1, backgroundColor: 'transparent' }} />
                <TouchableOpacity
                    activeOpacity={0.4}
                    style={itemStyle}
                    onPress={() => this.pushToDetails(item)}
                >
                    <View style={innerItem}>
                        <Text style={price}>{fare}</Text>
                        <Text style={durationStyle}>{`Duration: ${duration}`}</Text>
                        <Text style={timeStyle}>{time}</Text>
                    </View>
                </TouchableOpacity>
                <View style={dateContainer} >
                    <Text style={dateStyle}>{date}</Text>
                    <Text style={yearStyle}>{year}</Text>
                </View>
            </View>
        );
    }

    renderEmptyComponent() {
        if (this.state.loading) {
            return <ActivityIndicator color={color.maroon} />;
        }
        return <Text style={styles.noItemsText}>Sadly, you{'\''}ve made no bookings. Come back later when you{'\''}ve made some!</Text>;
    }

    render() {
        const {
            listContainer
        } = styles;
        return (
            <HamContainer
                title="History"
                navigate={this.props.navigation.navigate}
            >
                <FlatList
                    style={listContainer}
                    data={this.state.bookings}
                    renderItem={this.renderBookingBind}
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={this.renderEmptyComponentBind}
                    ListFooterComponent={() => {
                        if (this.state.bookings.length && this.state.loading) {
                            return <ActivityIndicator color={color.maroon} />;
                        }
                        return null;
                    }}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefreshBind}
                        />
                    }
                    onEndReached={this.getCallHistoryBind}
                    onEndReachedThreshold={0.9}
                />
            </HamContainer>
        );
    }
}

BookingList.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func
    }).isRequired,
    id: PropTypes.string.isRequired
};

const mapStateToProps = ({ user }) => ({
    id: user.id
});

const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        marginHorizontal: width / 30,
        backgroundColor: 'transparent'
    },
    listItem: {
        height: 145,
        backgroundColor: 'transparent',
        flexDirection: 'column',
        marginBottom: 15
    },
    itemStyle: {
        flex: 3.3,
        backgroundColor: color.mediumGrey,
        borderColor: color.grey,
        borderWidth: 0.5,
        borderRadius: width / 13
    },
    innerItem: {
        marginTop: 15,
        marginBottom: 15,
        backgroundColor: 'transparent',
        flex: 1,
        marginHorizontal: width / 13
    },
    price: {
        fontFamily: font.primary_mont_medium,
        fontSize: 20
    },
    durationStyle: {
        fontFamily: font.primary_mont,
        fontSize: 15,
        bottom: 0,
        position: 'absolute'
    },
    timeStyle: {
        fontFamily: font.primary_mont,
        position: 'absolute',
        right: 0,
        bottom: 0,
        fontSize: 15
    },
    dateContainer: {
        height: 95,
        width: 95,
        borderColor: color.grey,
        borderWidth: 0.5,
        borderRadius: 47.5,
        backgroundColor: 'white',
        right: width / 20,
        top: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        shadowColor: color.grey,
        shadowOffset: { width: 0, height: 10 },
        shadowOpacity: 1,
        shadowRadius: 15
    },
    dateStyle: {
        fontFamily: font.primary_mont_regular,
        fontSize: 18
    },
    yearStyle: {
        fontFamily: font.primary_mont,
        fontSize: 16
    },
    noItemsText: {
        fontFamily: font.primary_mont,
        marginTop: 50,
        textAlign: 'center'
    }
});

export default connect(mapStateToProps, null)(BookingList);
