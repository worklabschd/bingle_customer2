import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity,
    Image,
    ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import { IconInput, MeegoBackground } from '../../common';
import { font } from '../../theme';
import { MeegoLoginAPI, Constants } from '../../../api';
import { Snackbar, Utils } from '../../widgets';
import Logger from '../../../utils/logging';
import { DialogEvent } from '../../../events';

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            showEmail: false
        };
        this.email = '';
        const { goBack } = props.navigation;
        this.goBack = goBack;
        this.onSendBind = this.onSend.bind(this);
    }

    componentDidMount() {
        this.setState({
            showEmail: true
        }, () => this.emailView.fadeIn(650));
    }

    async onSend() {
        const email = this.email.trim();
        if (!email) {
            Utils.shakeAndDisplayToast(this.emailView, 'Email cannot be empty. Please try again.');
            return;
        }
        this.setState({
            loading: true
        });
        try {
            await MeegoLoginAPI.forgotPassword(email);
            this.setState({
                loading: false
            });
            await DialogEvent.alert('Email Sent', `An email has been successfully sent to ${this.email} regarding further instructions.`);
            this.goBack();
        } catch (err) {
            this.setState({
                loading: false
            });
            if (err === Constants.INVALID_EMAIL) {
                Utils.shakeAndDisplayToast(this.emailView, 'This email does not belong to any account. Please try again.');
                return;
            }
            Logger.warn(
                'Couldn\'t send forgot password.',
                'onSend:catch',
                {
                    email,
                },
                err
            );
            if (err === Constants.INTERNAL_SERVER_ERROR) {
                Snackbar.displayInternalServer();
                return;
            }
            Snackbar.displayNetworkError();
        }
    }

    render() {
        const {
            header,
            loginButton,
            buttonText,
            backButton,
            logoStyle,
            skipButton,
            headerImageContainer,
            footer
        } = styles;
        return (
            <MeegoBackground>
                <View style={header} >
                    <TouchableOpacity
                        style={skipButton}
                        onPress={() => this.goBack()}
                    >
                        <Image resizeMode="contain" style={backButton} source={require('../../../../assets/icons/left_arrow.png')} />
                    </TouchableOpacity>
                    <View style={headerImageContainer}>
                        <View style={{ flex: 0.94 }} />
                        <Image resizeMode="contain" style={logoStyle} source={require('../../../../assets/icons/white_logo.png')} />
                        <View style={{ flex: 0.5 }} />
                    </View>
                </View>
                <View
                    style={{
                        flex: 1
                    }}
                >
                    <View style={{ flex: 1 }} />
                    {this.state.showEmail ?
                        <IconInput
                            viewRef={component => { this.emailView = component; }}
                            image={
                                <Image
                                    source={require('../../../../assets/icons/envelope_white.png')}
                                    resizeMode="contain"
                                    style={{ flex: 1, width: 22 }}
                                />
                            }
                            placeholder="Email"
                            inputType="email-address"
                            onChangeText={value => { this.email = value; }}
                            onSubmit={this.onSendBind}
                        />
                        : null
                    }
                </View>
                <View style={{ flex: 1.48, flexDirection: 'row' }} >
                    <View style={{ flex: 0.02 }} />
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: 3.2 }} />
                        <TouchableOpacity
                            style={loginButton}
                            onPress={this.onSendBind}
                        >
                            {this.state.loading ?
                                <ActivityIndicator color="black" />
                                :
                                <Text
                                    style={buttonText}
                                >
                                  Proceed
                                </Text>
                            }
                        </TouchableOpacity>
                        <View style={footer} />
                    </View>
                    <View style={{ flex: 0.02 }} />
                </View>
            </MeegoBackground>
        );
    }
}

ForgotPassword.propTypes = {
    navigation: PropTypes.shape({
        goBack: PropTypes.func
    }).isRequired
};

const styles = StyleSheet.create({
    header: {
        flex: 1.5,
        flexDirection: 'column',
        alignItems: 'center'
    },
    loginButton: {
        borderRadius: 1,
        flex: Platform.OS === 'android' ? 1.5 : 1.2,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8.5,
        marginLeft: 8.5
    },
    buttonText: {
        fontFamily: font.primary,
        fontSize: 20
    },
    backButton: {
        width: 30,
        height: 30
    },
    logoStyle: {
        flex: 1,
        width: 130
    },
    skipButton: {
        flex: 0.4,
        backgroundColor: '#00000000',
        position: 'absolute',
        paddingLeft: 20.5,
        paddingTop: 30,
        left: 0,
        top: 20
    },
    headerImageContainer: {
        flex: 1.6,
        flexDirection: 'column'
    },
    footer: {
        flex: 1,
        backgroundColor: '#00000000',
        marginRight: 8.5,
        marginLeft: 8.5,
        alignItems: 'flex-end'
    }
});

export default ForgotPassword;
