import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    View,
    StyleSheet,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { HamContainer } from '../../common';
import { font, color } from '../../theme';
import { Utils } from '../../widgets';

const { width } = Dimensions.get('window');

const faqs = [['Bills', 'Money'], ['Guides', 'Bills'], ['Bills', 'Bills']];

class FAQ extends Component {
    constructor(props) {
        super(props);
        this.state = {
            somethingElse: '',
            submiting: false
        };
        this.submitingBind = this.submit.bind(this);
    }

    submit() {
        if (!this.state.somethingElse.trim()) {
            Utils.shakeAndDisplayToast(this.somethingElseContainer, 'Type in a question to continue.');
            return;
        }
        this.setState({
            submiting: true
        });
        setTimeout(() => {
            this.setState({
                submiting: false,
                somethingElse: ''
            });
        }, 2000);
    }

    render() {
        const {
            heading,
            headingText,
            headingTextLower,
            labels,
            labelRow,
            label,
            labelText,
            somethingElse,
            somethingElseLabel,
            somethingElseInput,
            somethingElseSubmit,
            somethingElseSubmitBtn,
            somethingElseSubmitText
        } = styles;
        const { navigate } = this.props.navigation;
        return (
            <HamContainer
                title="FAQ"
                navigate={navigate}
            >
                <KeyboardAvoidingView
                    behavior="position"
                    style={{ flex: 1 }}
                    contentContainerStyle={{ flex: 1 }}
                >
                    <View style={heading} >
                        <View style={{ flex: 0.5 }} />
                        <Text style={headingText}>
                      Aenean eu nisl lacinia, aliquam ipsum vitae, pretium ligula?
                        </Text>
                        <Text style={headingTextLower}>
                      Cras suscipit porta ante, sed rhoncus lectus placerat eget.
                        </Text>
                        <View style={{ flex: 0.4 }} />
                        <Text style={headingText}>
                      Etiam nunc tortor, feugiat a semper sit amet, pellentesque eu sem?
                        </Text>
                        <Text style={headingTextLower}>
                      Cras suscipit porta ante, sed rhoncus lectus placerat eget...
                        </Text>
                        <View style={{ flex: 1 }} />
                    </View>
                    <View style={labels} >
                        {faqs.map(faqPair => (
                            <View style={labelRow}>
                                {faqPair.map(faq => (
                                    <TouchableOpacity
                                        style={label}
                                        onPress={() => navigate('FAQDetail', { category: faq })}
                                    >
                                        <Text style={labelText}>{faq}</Text>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        ))}
                    </View>
                    <View style={somethingElse} >
                        <View style={{ flex: 0.2 }} />
                        <Text style={somethingElseLabel}>Something else? Ask here.</Text>
                        <View style={{ flex: 0.1 }} />
                        <Animatable.View
                            ref={component => { this.somethingElseContainer = component; }}
                            style={{ flex: 1.2 }}
                        >
                            <TextInput
                                underlineColorAndroid="#00000000"
                                style={somethingElseInput}
                                multiline
                                maxLength={100}
                                numberOfLines={3}
                                placeholder="Enter your question here"
                                value={this.state.somethingElse}
                                onChangeText={value => this.setState({
                                    somethingElse: value
                                })}
                            />
                        </Animatable.View>
                        <View style={{ flex: 0.2 }} />
                        <View style={somethingElseSubmit}>
                            <TouchableOpacity
                                disabled={this.state.submiting}
                                style={somethingElseSubmitBtn}
                                onPress={this.submitingBind}
                            >
                                {
                                    !this.state.submiting ?
                                        <Text style={somethingElseSubmitText}>Submit</Text>
                                        : <ActivityIndicator color="black" />
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.2 }} />
                    </View>
                </KeyboardAvoidingView>
            </HamContainer>
        );
    }
}

const styles = StyleSheet.create({
    heading: {
        flex: 0.95,
        marginLeft: width / 15,
        marginRight: width / 15
    },
    headingText: {
        fontFamily: font.primary_mont,
        fontSize: 16
    },
    headingTextLower: {
        fontFamily: font.primary_mont_semi_bold,
        fontSize: 15
    },
    labels: {
        flex: 0.9,
        flexDirection: 'column',
        marginLeft: width / 25,
        marginRight: width / 25
    },
    labelRow: {
        flex: 1,
        flexDirection: 'row'
    },
    label: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: color.mediumGrey,
        margin: 1.5,
        borderRadius: width / 23,
        borderColor: color.grey,
        borderWidth: 0.5
    },
    labelText: {
        fontFamily: font.primary_mont,
        fontSize: 16
    },
    somethingElse: {
        flex: 0.85,
        flexDirection: 'column',
        marginLeft: width / 25,
        marginRight: width / 25
    },
    somethingElseLabel: {
        fontFamily: font.primary_mont_regular,
        fontSize: 14,
        paddingLeft: width / 45
    },
    somethingElseInput: {
        fontFamily: font.primary_mont_regular,
        fontSize: 14,
        flex: 1,
        borderRadius: width / 23,
        borderColor: color.grey,
        borderWidth: 1,
        paddingLeft: width / 25,
        paddingRight: width / 25
    },
    somethingElseSubmitBtn: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: color.green,
        borderRadius: width / 14,
        borderColor: color.grey,
        borderWidth: 1,
    },
    somethingElseSubmit: {
        flex: 0.75
    },
    somethingElseSubmitText: {
        fontFamily: font.primary_mont_medium,
        fontSize: 17,
        alignSelf: 'center'
    }
});

FAQ.propTypes = {
    navigation: PropTypes.shape({
        goBack: PropTypes.func,
        navigate: PropTypes.func
    }).isRequired
};

export default FAQ;
