import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    BackHandler,
    KeyboardAvoidingView,
    TextInput
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import PropTypes from 'prop-types';
import { Font } from 'expo';
import { NavigationActions } from 'react-navigation';
import { font, color } from '../../theme';
import { Snackbar, Utils } from '../../widgets';
import { HamContainer } from '../../common';

class Feedback extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            feedbackText: null,
        };

       
        const { goBack, dispatch, state: { params } } = props.navigation;
        this.goBack = goBack;
       
    }

    componentDidMount() {
        Font.loadAsync({
          'intro-bold': require('../../../../assets/fonts/Intro-Bold.otf'),
          'intro-regular-alt': require('../../../../assets/fonts/Intro-Regular-Alt.otf'),
        });
    }

    render() {
        
       return (
            <HamContainer title='' iconColor navigate={this.props.navigation.navigate}>
                <View style={{flex: 1, justifyContent:'space-between', margin: 10, backgroundColor: color.white}}>
                    <View style={{flexDirection:'column', marginTop:10}}>
                        <Text style={{fontSize: 25, fontFamily: font.primary_mont_bold}}>
                            Send Feedback
                        </Text>
                        <Text style={{fontSize: 18, color:color.darkgrey, fontFamily: font.primary_mont_regular}}>
                            Tell us what you love about the app, or what we could be doing better.
                        </Text>
                        <TextInput
                            style={{marginHorizontal:10, marginVertical:15, fontSize:18, fontFamily:font.primary_mont_semi_bold, height:60, borderBottomWidth:1, borderBottomColor:color.lightGrey, textAlignVertical:'center', borderColor: 'transparent',}}
                            onChangeText={(text) => this.setState({feedbackText:text})}
                            value={this.state.feedbackText}
                            placeholder='Enter feedback'
                            placeholderColor={color.darkgrey}
                            underlineColorAndroid='transparent'
                        />
                        <Text style={{fontSize: 15, color:color.darkgrey, fontFamily: font.primary_mont_regular}}>
                            If you have more detailed feedback to share, good and bad, we want it all. Just drop in an email to{' '}
                            <Text style={{fontSize: 15, color:color.red, fontFamily: font.primary_mont_regular}}>
                                support@bingle.com
                            </Text>
                        </Text>
                    </View>
                    <View style={{}}>
                        <TouchableOpacity style={{backgroundColor:color.mediumGrey, borderRadius:8,}}>
                            <Text style={{paddingVertical:20, textAlign:'center', fontSize:18, color:color.grey, fontFamily:font.primary_mont_medium }}>
                                Submit 
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </HamContainer>
       );
    }
}

Feedback.propTypes = {
    navigation: PropTypes.shape({

    }).isRequired
};

const styles = StyleSheet.create({

});

export default Feedback;
