import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
// import { connect } from 'react-redux';
import HamContainer from '../../common/HamContainer';
import { color, font } from '../../../components/theme/index';

class AddPayment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '0',
            name: props.navigation.state.params.obj[0].name,
            number: props.navigation.state.params.obj[0].number,
            bal: props.navigation.state.params.obj[0].bal,
        };
    }
    convertString(str) {
        let newString = str.slice(4, 12);
        newString = `XXXX${newString}`;
        return newString;
    }
    render() {
        return (
            <HamContainer title="Payment Method" navigate={this.props.navigation.goBack} backEnabled>
                <View style={styles.container}>
                    <View>
                        <View style={styles.methodContainer}>
                            <TouchableOpacity
                                onPress={() => { }}
                                style={styles.methods}
                            >
                                <View style={{ flex: 1 }}>
                                    <Text style={[styles.text, styles.heading]}>
                                        {this.state.name}
                                    </Text>
                                    <Text style={[styles.text, styles.number]}>
                                        {this.convertString(`${this.state.number}`)}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.addPaymentContainer}>
                            <TouchableOpacity
                                onPress={() => { }}
                            >
                                <Text style={[styles.text, styles.number]}> Change Account</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.addPaymentContainer}>
                            <TouchableOpacity
                                onPress={() => { }}
                            >
                                <Text style={[styles.text, styles.number]}> Account Settings</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity style={styles.button}>
                        <Text style={[styles.text, styles.number]}>Update Account</Text>
                    </TouchableOpacity>
                </View>
            </HamContainer>

        );
    }
}
// const mapStateToProps = ({ methods }) => ({
//     name: methods.name, bal: methods.bal, number: methods.number
// });

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 4,
        borderColor: '#d6d7da',
        paddingVertical: '10%'
    },
    methodContainer: {
        flexDirection: 'row',
        paddingHorizontal: '5%'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },

    methods: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        alignItems: 'stretch',
        // paddingHorizontal: '10%',
        borderBottomWidth: 1,
        paddingVertical: '2%',
        padding: 0,
        borderBottomColor: 'grey',
        // backgroundColor: 'red'

    },
    addPaymentContainer: {
        width: '80%',
        paddingVertical: '2%',
        paddingHorizontal: '3.5%'
    },
    text: {
        fontFamily: font.primary_mont,
    },
    heading: {
        fontSize: 13,
    },
    number: {
        fontSize: 15,
        fontFamily: font.primary_mont_medium
    },
    current: {
        height: 8,
        width: 8,
        backgroundColor: color.green,
        borderRadius: 4,
        alignSelf: 'center'
    },
    button: {
        backgroundColor: color.green,
        height: 60,
        width: '80%',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 0,

    }
});
// export default connect(mapStateToProps, null)(AddPayment);
export default AddPayment;
