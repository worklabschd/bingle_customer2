import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import HamContainer from '../../common/HamContainer';
import { color, font } from '../../../components/theme/index';

class Payment extends React.Component {
    convertString(str) {
        let newString = str.slice(4, 12);
        newString = `XXXX${newString}`;
        return newString;
    }
    componentWillReceiveProps(nextProps) {
        console.warn(JSON.stringify(nextProps));
    }
    renderMethods() {
        return this.props.methods.map((item) => (
            <View style={styles.methodContainer}>
                <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate('AddAccount', { obj: [item] }); }}
                    style={[styles.methods, { width: '82%' }]}
                >
                    <View style={{ flex: 1 }}>
                        <Text style={[styles.text, styles.heading]}>
                            {item.name}
                        </Text>
                        <Text style={[styles.text, styles.number]}>
                            {this.convertString(item.number.toString())}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        ));
    }
    render() {
        return (
            <HamContainer title="Payment Method" navigate={this.props.navigation.navigate}>
                <View style={styles.container}>
                    <View style={styles.methodContainer}>
                        <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate('AddPayment'); }}
                            style={styles.methods}
                        >
                            <View style={{ flex: 1 }}>
                                <Text style={[styles.text, styles.heading]}>
                                    Payment Wallet
                                </Text>
                                <Text style={[styles.text, styles.number]}>
                                    {this.props.number}
                                </Text>
                            </View>
                            <View style={{
                                borderWidth: 1, borderColor: 'grey', height: '90%', marginRight: '5%', marginBottom: '5%',
                            }}
                            />
                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
                                <View>
                                    <Text style={[styles.text, styles.heading]}>
                                        Current Balance
                                    </Text>
                                    <Text style={[styles.text, styles.number]}>
                                        {this.props.bal}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.current} />

                    </View>
                    {this.renderMethods()}
                    <View style={styles.addPaymentContainer}>
                        <Text style={[styles.text, styles.heading]}>Add payment methods</Text>
                        <TouchableOpacity >
                            <Text style={[styles.text, styles.number]}>UPI</Text>
                        </TouchableOpacity>
                        <TouchableOpacity >
                            <Text style={[styles.text, styles.number]}>Credit or Debit Card</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </HamContainer>

        );
    }
}

const mapStateToProps = ({ methods, payment }) => ({
    bal: payment.bal, number: payment.number, methods
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        borderRadius: 4,
        borderColor: '#d6d7da',
        paddingVertical: '10%'
    },
    methodContainer: {
        flexDirection: 'row'
    },
    methods: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '80%',
        alignItems: 'stretch',
        // paddingHorizontal: '10%',
        borderBottomWidth: 1,
        paddingVertical: '2%',
        padding: 0,
        borderBottomColor: 'grey',
        // backgroundColor: 'red'

    },
    addPaymentContainer: {
        width: '82%',
        paddingVertical: '2%',
    },
    text: {
        fontFamily: font.primary_mont,
    },
    heading: {
        fontSize: 13,
    },
    number: {
        fontSize: 15,
        fontFamily: font.primary_mont_medium
    },
    current: {
        height: 8,
        width: 8,
        backgroundColor: color.green,
        borderRadius: 4,
        alignSelf: 'center'
    }
});
// export default Payment;
export default connect(mapStateToProps, null)(Payment);
