import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import { connect } from 'react-redux';
import HamContainer from '../../common/HamContainer';
import { color, font } from '../../../components/theme/index';

class AddPayment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '0',
            number: props.number,
            bal: props.bal,
        };
    }
    render() {
        return (
            <HamContainer title="Payment Method" navigate={this.props.navigation.goBack} backEnabled>
                <View style={styles.container}>
                    <View>
                        <View style={styles.methodContainer}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => { }}
                                style={styles.methods}
                            >
                                <View style={{ flex: 1 }}>
                                    <Text style={[styles.text, styles.heading]}>
                                    Payment Wallet
                                    </Text>
                                    <Text style={[styles.text, styles.number]}>
                                        {this.state.number}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <View style={styles.current} />

                        </View>
                        <View style={styles.methodContainer}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => { }}
                                style={styles.methods}
                            >
                                <View style={{ flex: 1 }}>
                                    <Text style={[styles.text, styles.heading]}>
                                    Current Balance
                                    </Text>
                                    <View style={styles.inputContainer}>
                                        <Text style={[styles.text, styles.number]} />
                                        <TextInput editable={false} value={this.state.bal} underlineColorAndroid="transparent" style={[{ width: '100%' }, styles.number]} />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.addPaymentContainer}>
                            <Text style={[styles.text, styles.heading]}> Add Money</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={styles.button}>
                        <Text style={[styles.text, styles.number]}>Update Account</Text>
                    </TouchableOpacity>
                </View>
            </HamContainer>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: '#d6d7da',
        paddingVertical: '10%'
    },
    methodContainer: {
        flexDirection: 'row'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },

    methods: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '90%',
        alignItems: 'stretch',
        // paddingHorizontal: '10%',
        borderBottomWidth: 1,
        paddingVertical: '2%',
        marginHorizontal: '5%',
        borderBottomColor: 'grey',
        // backgroundColor: 'red'

    },
    addPaymentContainer: {
        width: '80%',
        paddingVertical: '2%',
        paddingHorizontal: '5%'
    },
    text: {
        fontFamily: font.primary_mont,
    },
    heading: {
        fontSize: 13,
    },
    number: {
        fontSize: 15,
        fontFamily: font.primary_mont_medium
    },
    current: {
        height: 8,
        width: 8,
        backgroundColor: color.green,
        borderRadius: 4,
        alignSelf: 'center'
    },
    button: {
        backgroundColor: color.green,
        height: 60,
        width: '80%',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        bottom: 10
    }
});
const mapStateToProps = ({ payment }) => ({
    ...payment
});

export default connect(mapStateToProps, null)(AddPayment);
