import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text,
    WebView,
    StyleSheet,
    BackHandler,
    TouchableOpacity,
    ActivityIndicator,
    Linking,
    Platform
} from 'react-native';
import { WebBrowser, Constants as ExpoConstants } from 'expo';
import { connect } from 'react-redux';
import { HamContainer } from '../../common';
import { font, color } from '../../theme';
import getPaymentForm from './html';
import { PaymentAPI, Constants } from '../../../api';
import { LoadingEvent, DialogEvent } from '../../../events';
import Logger from '../../../utils/logging';
import { SERVER_URL } from '../../../utils/constants';
import { Snackbar } from '../../widgets';

// fix https://github.com/facebook/react-native/issues/10865
/* eslint-disable */
const patchPostMessageJsCode = `(${String(() => {
    const originalPostMessage = window.postMessage;
    const patchedPostMessage = function (message, targetOrigin, transfer) {
        originalPostMessage(message, targetOrigin, transfer);
    };
    patchedPostMessage.toString = function () {
        return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
    };
    window.postMessage = patchedPostMessage;
})})();`;
/* eslint-enable */

class Payment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showWebview: false,
            bookings: 0,
            due: 0,
            paid: 0,
            receiving: false
        };
        this.onPayBind = this.onPay.bind(this);
        this.onReceivedBind = this.onReceived.bind(this);
        this.onHardwareBackPressBind = this.onHardwareBackPress.bind(this);
        this.handleDeeplinkBind = this.handleDeeplink.bind(this);
        this.useExpoBrowser = true;
    }

    onHardwareBackPress() {
        if (this.state.showWebview) {
            this.setState({
                showWebview: false
            });
            return true;
        }
        return false;
    }

    handleDeeplink({ url }) {
        WebBrowser.dismissBrowser();
        this.onReceived({ nativeEvent: { data: url.split('payment=')[1] } });
    }

    async componentDidMount() {
        Linking.addEventListener('url', this.handleDeeplinkBind);
        BackHandler.addEventListener('hardwareBackPress', this.onHardwareBackPressBind);
        LoadingEvent.show();
        try {
            const { bookings, paid, due } = await PaymentAPI.getDue(this.props.id);
            this.setState({
                paid,
                bookings,
                due
            });
        } catch (err) {
            if (err === Constants.INTERNAL_SERVER_ERROR) {
                Snackbar.displayInternalServer();
                return;
            }
            Snackbar.displayNetworkError();
        }
        LoadingEvent.cancel();
    }

    componentWillUnmount() {
        Linking.removeEventListener('url', this.handleDeeplinkBind);
        BackHandler.removeEventListener('hardwareBackPress', this.onHardwareBackPressBind);
    }

    onPay() {
        if (this.useExpoBrowser) {
            const {
                name,
                phone,
                email
            } = this.props;
            if (Platform.OS === 'ios') {
                Linking.openURL(`${SERVER_URL}/make-payment?link=${ExpoConstants.linkingUri}&name=${name}&email=${email}&phone=${phone}&amount=${this.state.due}&paymentGateway=razorpay`);
                return;
            }
            WebBrowser.openAuthSessionAsync(`${SERVER_URL}/make-payment?link=${ExpoConstants.linkingUri}&name=${name}&email=${email}&phone=${phone}&amount=${this.state.due}&paymentGateway=razorpay`);
            return;
        }
        this.setState({
            showWebview: true
        });
    }

    async onReceived({ nativeEvent: { data } }) {
        this.setState({
            showWebview: false
        });
        if (!data) {
            Snackbar.displayToast('You cancelled this payment. Please try again.');
            return;
        }
        this.setState({
            receiving: true
        });
        const { due, paid } = this.state;
        try {
            await PaymentAPI.makePayment(this.props.id, data, due, 'razorpay');
            this.setState({
                receiving: false,
                due: 0,
                paid: due + paid
            });
            DialogEvent.alert('Success!', `Payment of ₹${due} successfully made! Please go to dashboard to make more bookings.`);
        } catch (err) {
            this.setState({
                receiving: false
            });
            Logger.warn(
                'Couldn\'t process payment.',
                'onReceived:catch',
                Object.assign({}, { id: this.props.id, paymentID: data, due }),
                err
            );
            DialogEvent.alert('Error!', 'Could not process this payment, sadly. If we\'ve deducted amount from your account. It will be refunded within the next 5 days.');
        }
    }

    render() {
        const {
            container,
            headingContainer,
            heading,
            headingTwo,
            body,
            dueStyle,
            button
        } = styles;
        const {
            paid,
            bookings,
            due,
            receiving,
            showWebview
        } = this.state;
        const {
            name,
            email,
            phone,
            navigation: { navigate }
        } = this.props;
        if (showWebview) {
            return (
                <WebView
                    onLoadStart={(navState) => console.log(navState)}
                    onNavigationStateChange={e => console.log(e)}
                    source={{
                        html: getPaymentForm(due, name, email, phone)
                    }}
                    style={{ marginTop: 20 }}
                    onMessage={this.onReceivedBind}
                />
            );
        }

        return (
            <HamContainer
                title="Make Payment"
                navigate={navigate}
            >
                <View style={container} >
                    <View style={headingContainer}>
                        <Text style={heading}>Here you can clear all your dues with Meego.</Text>
                        <Text style={headingTwo}>
                            Total number of bookings - {bookings}.
                        </Text>
                        <Text style={headingTwo}>
                            Total amount paid - ₹{paid}.
                        </Text>
                    </View>
                    <View style={body}>
                        <Text style={heading}>Amount Due</Text>
                        <Text style={dueStyle}>₹{due}</Text>
                    </View>
                    <TouchableOpacity
                        style={[button, { backgroundColor: due ? color.green : color.mediumGrey }]}
                        disabled={receiving || !due}
                        onPress={this.onPayBind}
                    >
                        {
                            receiving ?
                                <ActivityIndicator color="black" />
                                :
                                <Text style={[headingTwo, { paddingBottom: 0 }]}>{due ? 'Pay' : 'Nothing to Pay'}</Text>
                        }
                    </TouchableOpacity>
                </View>
            </HamContainer>
        );
    }
}

Payment.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func
    }).isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headingContainer: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center'
    },
    heading: {
        fontFamily: font.primary_mont,
        fontSize: 14,
        paddingTop: 10,
        paddingBottom: 20
    },
    headingTwo: {
        fontFamily: font.primary_mont_medium,
        fontSize: 14,
        paddingBottom: 10
    },
    body: {
        flex: 0.8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    dueStyle: {
        fontFamily: font.primary_mont_medium,
        fontSize: 20
    },
    button: {
        height: 60,
        width: '80%',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 20
    }
});

const mapStateToProps = ({
    user: {
        id, name, email, phone
    }
}) => ({
    id,
    name,
    email,
    phone
});

export default connect(mapStateToProps, null)(Payment);
