import React from "react";
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { color, font } from "../../theme";
import {
  GiftedChat,
  Actions,
  Bubble,
  SystemMessage
} from "react-native-gifted-chat";
import Modal from "react-native-modal";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";

class ChatScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
      visibleModal: false,
      radioIndex: 0,
      showOptions: true
    };

    this._isMounted = false;
    this.onSend = this.onSend.bind(this);
    this.onReceive = this.onReceive.bind(this);
    this.renderBubble = this.renderBubble.bind(this);
    this.renderFooter = this.renderFooter.bind(this);

    this._isAlright = null;
  }

  componentWillMount() {
    this._isMounted = true;
    this.setState(() => {
      return {
        messages: require("./messages.js")
      };
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onSend(messages = []) {
    this.setState(previousState => {
      return {
        messages: GiftedChat.append(previousState.messages, messages)
      };
    });
  }

  onReceive(text) {
    this.setState(previousState => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(),
          user: {
            _id: 2,
            name: "React Native"
            // avatar: 'https://facebook.github.io/react/img/logo_og.png',
          }
        })
      };
    });
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: "#ffffff"
          },
          right: {
            backgroundColor: "#a0db8e"
          }
        }}
      />
    );
  }

  renderFooter(props) {
    if (this.state.typingText) {
      return (
        <View style={styles.footerContainer}>
          <Text style={styles.footerText}>{this.state.typingText}</Text>
        </View>
      );
    }
    return null;
  }

  render() {
    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === "ios" ? "position" : "padding"}
        enabled
      >
        <View style={{ flex: 1, flexDirection: "column" }}>
          <Modal
            isVisible={this.state.visibleModal}
            onBackdropPress={() => this.setState({ visibleModal: false })}
          >
            <View style={styles.modalContent}>
              <RadioGroup
                size={25}
                selectedIndex={0}
                color="#000"
                //onSelect = {(index, value) => this.onSelectRadio(index, value)}
                onSelect={(index, value) =>
                  this.setState({ radioIndex: index, visibleModal: false })
                }
              >
                <RadioButton value={"1"}>
                  <Text>Do not want to chat</Text>
                </RadioButton>
                <RadioButton value={"2"}>
                  <Text>Busy</Text>
                </RadioButton>
                <RadioButton value={"3"}>
                  <Text>Sending Wrong Messages</Text>
                </RadioButton>
              </RadioGroup>

              <Text style={styles.text}>{this.state.text}</Text>
            </View>
          </Modal>

          <View style={{ height: 23, backgroundColor: "#2980b6" }} />
          <View
            style={{
              height: 60,
              alignItems: "center",
              flexDirection: "row",
              justifyContent: "flex-start",
              backgroundColor: "white"
            }}
          >
            <View style={{ alignItems: "flex-start" }}>
              <TouchableOpacity
                hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}
                style={{ marginLeft: 20 }}
                onPress={() => {
                  this.props.navigation.goBack();
                }}
              >
                <Ionicons
                  name={"ios-arrow-back"}
                  size={30}
                  color={color.blue}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                marginHorizontal: 30,
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <View style={{ borderRadius: 50, overflow: "hidden" }}>
                <Image
                  source={require("../../../../assets/icons/avatar.jpg")}
                  style={{ height: 45, width: 45 }}
                  resizeMode="stretch"
                />
              </View>

              <View style={{ marginHorizontal: 15 }}>
                <Text
                  style={{
                    textAlignVertical: "center",
                    fontFamily: font.primary_mont_semi_bold,
                    fontSize: 18,
                    textAlign: "center"
                  }}
                >
                  Sandeep
                </Text>
                <Text
                  style={{
                    textAlignVertical: "center",
                    fontFamily: font.primary_mont_medium,
                    fontSize: 14,
                    textAlign: "center",
                    color: color.grey
                  }}
                >
                  online
                </Text>
              </View>
            </View>
          </View>
          <GiftedChat
            messages={this.state.messages}
            onSend={this.onSend}
            user={{
              _id: 1 // sent messages should have same user._id
            }}
            renderBubble={this.renderBubble}
            renderFooter={this.renderFooter}
          />

          {this.state.showOptions ? (
            <View
              style={{
                height: 100,
                justifyContent: "center",
                width: "100%",
                backgroundColor: "#FFFFFF",
                position: "absolute",
                top: Dimensions.get("window").height * 0.3 - 10
              }}
            >
              <View style={{ alignItems: "center" }}>
                <TouchableOpacity
                  style={{ flexDirection: "row" }}
                  onPress={() => this.setState({ showOptions: false })}
                >
                  <Image
                    style={{ marginVertical: 10 }}
                    source={require("../../../../assets/icons/lets.png")}
                  />
                  <Text
                    style={{
                      textAlign: "left",
                      marginVertical: 10,
                      marginHorizontal: 5
                    }}
                  >
                    Let's Chat
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{ flexDirection: "row" }}
                  onPress={() => this.setState({ visibleModal: true })}
                >
                  <Image
                    style={{ marginVertical: 10 }}
                    source={require("../../../../assets/icons/block.png")}
                  />
                  <Text
                    style={{
                      textAlign: "left",
                      marginVertical: 10,
                      marginHorizontal: 10
                    }}
                  >
                    Block
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  footerText: {
    fontSize: 14,
    color: "#aaa"
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "flex-start",
    borderRadius: 8,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
});

export default ChatScreen;
