import React from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import { TabNavigator } from "react-navigation";
import { SimpleLineIcons } from "@expo/vector-icons";
import { color, font } from "../../theme";
import ChatScreen from "./chatScreen";
import { StackNavigator } from "react-navigation";

class ActiveUsers extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          flexDirection: "column",
          backgroundColor: "white",
          paddingTop: 20,
          borderTopColor: "#00b4ff",
          borderTopWidth: 1,
          borderStyle: "solid"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginLeft: 10,
            marginRight: 10
          }}
        >
          <Image
            style={{
              width: 60,
              height: 60,
              borderRadius: 50,
              marginRight: 10,
              marginLeft: 15
            }}
            source={{
              uri:
                "https://facebook.github.io/react-native/docs/assets/favicon.png"
            }}
          />
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              borderBottomColor: "#00b4ff",
              borderBottomWidth: 1,
              borderStyle: "solid",
              paddingBottom: 30,
              marginLeft: 10,
              paddingRight: 10
            }}
          >
            <Text
              style={{
                fontSize: 18,
                fontFamily: font.primary_mont_semi_bold
              }}
            >
              User 1
            </Text>
            <SimpleLineIcons name="arrow-right" size={20} color={color.grey} />
          </View>
        </View>
      </View>
    );
  }
}

class UsersList extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          flexDirection: "column",
          backgroundColor: "white",
          paddingTop: 20,
          borderTopColor: "#00b4ff",
          borderTopWidth: 1,
          borderStyle: "solid"
        }}
      >
        <TouchableOpacity
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginLeft: 10,
            marginRight: 10
          }}
        >
          <Image
            style={{
              width: 60,
              height: 60,
              borderRadius: 50,
              marginRight: 10,
              marginLeft: 15
            }}
            source={{
              uri:
                "https://facebook.github.io/react-native/docs/assets/favicon.png"
            }}
          />
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              borderBottomColor: "#00b4ff",
              borderBottomWidth: 1,
              borderStyle: "solid",
              paddingBottom: 20,
              marginLeft: 10,
              paddingRight: 10
            }}
          >
            <View>
              <Text
                style={{
                  fontSize: 18,
                  fontFamily: font.primary_mont_semi_bold
                }}
              >
                User 1
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: font.primary_mont_semi_bold,
                  color: color.grey
                }}
              >
                Hi, how are you?
              </Text>
            </View>
            <Text
              style={{
                fontSize: 16,
                fontFamily: font.primary_mont_semi_bold,
                color: color.grey,
                alignSelf: "flex-start"
              }}
            >
              10:23 PM
            </Text>
            <SimpleLineIcons name="arrow-right" size={20} color={color.grey} />
          </View>
        </TouchableOpacity>

        {/* hello */}

        <View style={{ height: 5 }} />

        <TouchableOpacity
          onPress={() => navigate("ChatScreen")}
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginLeft: 10,
            marginRight: 10
          }}
        >
          <Image
            style={{
              width: 60,
              height: 60,
              borderRadius: 50,
              marginRight: 10,
              marginLeft: 15
            }}
            source={{
              uri:
                "https://facebook.github.io/react-native/docs/assets/favicon.png"
            }}
          />
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              borderBottomColor: "#00b4ff",
              borderBottomWidth: 1,
              borderStyle: "solid",
              paddingBottom: 20,
              marginLeft: 10,
              paddingRight: 10
            }}
          >
            <View>
              <Text
                style={{
                  fontSize: 18,
                  fontFamily: font.primary_mont_semi_bold
                }}
              >
                User 1
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: font.primary_mont_semi_bold,
                  color: color.grey
                }}
              >
                Hi, how are you?
              </Text>
            </View>
            <Text
              style={{
                fontSize: 16,
                fontFamily: font.primary_mont_semi_bold,
                color: color.grey,
                alignSelf: "flex-start"
              }}
            >
              10:23 PM
            </Text>
            <SimpleLineIcons name="arrow-right" size={20} color={color.grey} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default TabNavigator(
  {
    "Active Users": { screen: ActiveUsers },
    "Users List": { screen: UsersList }
  },
  {
    tabBarPosition: "top",
    tabBarOptions: {
      inactiveTintColor: "#fff",
      activeTintColor: "#00b4ff",
      labelStyle: {
        fontSize: 16,
        fontFamily: font.primary_mont_bold,
        padding: 0,
        margin: 0,
        backgroundColor: "transparent"
      },
      style: {
        backgroundColor: "#00b4ff",
        borderRadius: 50,
        width: "80%",
        alignSelf: "center",
        padding: 0,
        marginTop: 10,
        marginBottom: 15,
        justifyContent: "center",
        elevation: 0
      },
      upperCaseLabel: false,
      indicatorStyle: {
        backgroundColor: "transparent"
      },
      tabStyle: {
        backgroundColor: "#fff",
        margin: 5,
        padding: 10,
        borderBottomWidth: 0,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50
      },
      inactiveBackgroundColor: "transparent"
    }
  }
);
