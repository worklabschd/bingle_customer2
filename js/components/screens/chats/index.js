import React from "react";
import { Text, View, Image, TouchableOpacity, Platform } from "react-native";
import { TabNavigator } from "react-navigation";
import { SimpleLineIcons } from "@expo/vector-icons";
import { color, font } from "../../theme";
import { StackNavigator } from "react-navigation";
import UsersList from "./UsersList";
import ActiveUsers from "./ActiveUsers";
import { HamContainer } from "../../common";
import { Ionicons } from "@expo/vector-icons";
import ChatScreen from "./ChatScreen";

const tabNav = TabNavigator(
  {
    "Active Users": { screen: ActiveUsers },
    "Users List": { screen: UsersList }
  },
  {
    tabBarPosition: "top",
    backgroundColor: "white",
    tabBarOptions: {
      inactiveTintColor: "#D3D3D3",
      activeTintColor: "#00b4ff",
      labelStyle: {
        fontSize: 16,
        fontFamily: font.primary_mont_bold,
        padding: 0,
        margin: 0,
        backgroundColor: "transparent"
      },
      style: {
        backgroundColor: "#00b4ff",
        borderRadius: 50,
        width: "90%",
        alignSelf: "center",
        padding: 0,
        marginTop: 10,
        marginBottom: 15,
        justifyContent: "center",
        elevation: 0
      },
      upperCaseLabel: false,
      indicatorStyle: {
        backgroundColor: "transparent"
      },
      tabStyle: {
        backgroundColor: "#fff",
        margin: 5,
        padding: 10,
        borderBottomWidth: 0,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50
      },
      inactiveBackgroundColor: "transparent"
    }
  }
);

const Chats = StackNavigator({
  Home: {
    screen: tabNav,
    navigationOptions: ({ navigation }) => ({
      header: (
        <View
          style={{
            flexDirection: "column",
            backgroundColor: "white",
            height: 125,
            paddingTop: 30

            // only for IOS to give StatusBar Space
          }}
        >
          <View
            style={{
              alignItems: "center",
              flexDirection: "row",
              justifyContent: "space-between",
              backgroundColor: "white"
            }}
          >
            <View style={{ alignItems: "flex-start" }}>
              <TouchableOpacity
                hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}
                style={{ marginLeft: 20 }}
                onPress={() => {
                  navigation.navigate("DrawerOpen");
                }}
              >
                <Ionicons name={"md-menu"} size={30} color={"#00D6ff"} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              style={{
                fontSize: 24,
                textAlign: "center",
                fontFamily: font.primary_mont_semi_bold
              }}
            >
              The Next Door Cafe!
            </Text>
          </View>
        </View>
      )
    })
  },
  ChatScreen: { screen: ChatScreen }
});

export default Chats;
