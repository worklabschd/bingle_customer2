import React from "react";
import {
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
import { TabNavigator } from "react-navigation";
import { SimpleLineIcons } from "@expo/vector-icons";
import { color, font } from "../../theme";
import { GiftedChat } from "react-native-gifted-chat";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { messages: [] };
    this.onSend = this.onSend.bind(this);
  }

  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: "Hello developer",
          createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
          user: {
            _id: 2,
            name: "React Native",
            avatar: "https://facebook.github.io/react/img/logo_og.png"
          }
        }
      ]
    });
  }

  onSend(messages = []) {
    this.setState(previousState => {
      return {
        messages: GiftedChat.append(previousState.messages, messages)
      };
    });
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <GiftedChat
          showAvatarForEveryMessage={true}
          alwaysShowSend={true}
          showUserAvatar={true}
          messages={this.state.messages}
          onSend={this.onSend}
          user={{
            _id: 1
          }}
        />
        <View
          style={{
            height: 100,
            justifyContent: "center",
            width: "100%",
            backgroundColor: "#FFFFFF",
            position: "absolute",
            top: Dimensions.get("window").height * 0.5 - 20
          }}
        >
          <View style={{ alignItems: "center" }}>
            <TouchableOpacity style={{ flexDirection: "row" }}>
              <Image
                style={{ marginVertical: 10 }}
                source={require("../../../../assets/images/lets.png")}
              />
              <Text
                style={{
                  textAlign: "left",
                  marginVertical: 10,
                  marginHorizontal: 5
                }}
              >
                Let's Chat
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ flexDirection: "row" }}>
              <Image
                style={{ marginVertical: 10 }}
                source={require("../../../../assets/images/block.png")}
              />
              <Text
                style={{
                  textAlign: "left",
                  marginVertical: 10,
                  marginHorizontal: 10
                }}
              >
                Block
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default ChatScreen;
