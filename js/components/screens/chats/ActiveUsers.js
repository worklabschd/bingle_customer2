import React from 'react';
import { Text, View, Image,TouchableOpacity, Platform } from 'react-native';
import { TabNavigator } from 'react-navigation';
import { SimpleLineIcons } from '@expo/vector-icons';
import { color, font } from '../../theme';


class ActiveUsers extends React.Component {
    
      render() {
    
        const{navigate}=this.props.navigation
        return (
          <View style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'column',
            backgroundColor: 'white',
            paddingTop: 20,
            borderTopColor: '#00b4ff',
            borderTopWidth: 1,
            borderStyle: 'solid'
          }}>

          <TouchableOpacity 
            onPress={() => this.props.navigation.navigate('ChatScreen')}
            style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginLeft: 10,
            marginRight: 10
          }}>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginLeft: 10,
              marginRight: 10
            }}>
               <View style={{borderRadius:50, overflow:'hidden'}}>
              <Image source={require('../../../../assets/icons/avatar.jpg')}
                    style={{ height:45, width:45}} resizeMode='stretch' />
              </View>
              
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                borderBottomColor: '#00b4ff',
                borderBottomWidth: 1,
                borderStyle: 'solid',
                paddingBottom: 30,
                marginLeft: 10,
                paddingRight: 10,
              }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: font.primary_mont_semi_bold
                }}>User 1</Text>
                <SimpleLineIcons name="arrow-right" size={20} color={color.grey} />
              </View>
            </View>
            </TouchableOpacity>
          </View>
        );
      }
    }

  export default ActiveUsers;
