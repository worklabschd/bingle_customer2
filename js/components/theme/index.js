const color = {
    primary: '#ffcc00',
    black: '#000000',
    facebook: '#3b5998',
    google: '#db3236',
    yellow: '#0066ff',
    aqua: '#00d6ff',
    maroon: '#002c80',
    darkgrey: '#888888',
    grey: '#c9c9ca',
    mediumGrey: '#E5E5E5',
    lightGrey: '#EAEAEA',
    green: '#98e5b0',
    blue: '#243E82',
    white: '#FFF',
    red: '#ff0000'
};

const font = {
    primary: 'avenir_regular',
    primary_mont: 'mont_light',
    primary_mont_regular: 'mont_regular',
    primary_mont_medium: 'mont_medium',
    primary_mont_bold: 'mont_bold',
    primary_mont_semi_bold: 'mont_semi_bold'
};

export { color, font };
