import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Image,
    Platform
} from 'react-native';

import { Badge } from 'react-native-elements';
import { color, font } from '../theme';
import { BASE_PATH,NEW_BASE_PATH } from '../../utils/constants';

const { width } = Dimensions.get('window');

const NearbyPlace = ({
    name, distance, imagePath, onExplore, description, state
}) => {


    const {
        container,
        imageContainer,
        nameContainer,
        nameStyle,
        infoStyle,
        infoContainerStyle,
        bodyContainer,
        distanceContainer,
        distanceStyle
    } = styles;


    return (
        <TouchableWithoutFeedback onPress={onExplore}>
            <View style={container} >
                <View style={imageContainer}>
                    <Image
                        source={{ uri: `${NEW_BASE_PATH}${imagePath}` }}
                        style={{
                            height: null,
                            width: null,
                            flex: 1,
                        }}
                    />
                </View>
                <View style={bodyContainer}>
                    <View style={nameContainer}>
                        <Text style={{color:'#07213d',fontSize:11, fontFamily: font.primary_mont_medium}}>Open now</Text>
                        <Text numberOfLines={1} style={nameStyle}>{name}</Text>
                        <Text numberOfLines={1} style={{color:color.darkgrey,fontSize:14,fontFamily: font.primary_mont_medium}}>{state}</Text>
                        <Text numberOfLines={1} style={infoStyle}>{description}</Text>

                        {/*<View style={{ flex: 1.3, flexDirection: 'row' }}>

                            <TouchableOpacity onPress={onExplore} style={infoContainerStyle}>
                                <Text style={infoStyle}>More info</Text>
                            </TouchableOpacity>
q
                            <View style={{ flex: 0.3 }} />
                        </View>*/}
                    </View>

                    <Badge
                            value={3.5}
                            containerStyle={{ backgroundColor: '#9ACD32' }}
                            textStyle={{ color: color.white, fontFamily: font.primary_mont_medium }}
                        />
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

NearbyPlace.propTypes = {
    name: PropTypes.string.isRequired,
    distance: PropTypes.string.isRequired,
    imagePath: PropTypes.string.isRequired,
    onExplore: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: {
        height: width / 3.8,
        borderRadius: 3,
        top:5,
        margin:10,
        marginTop:4,
        marginBottom:4,
        bottom:5,
        padding:12,
        backgroundColor: color.white,
        borderColor: color.grey,
        borderWidth: 0.5,
        flexDirection: 'row'
    },
    imageContainer: {
        flex: 1,
        borderRadius:5,
        overflow: 'hidden',
        borderColor: color.grey
    },
    bodyContainer: {
        flex: 3,
        flexDirection: 'row',
        paddingLeft: 10
    },
    nameContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: "space-around"
    },
    nameStyle: {
        fontFamily: font.primary_mont_medium,
        fontSize: 18,
        color: font.darkgrey
    },
    infoContainerStyle: {
        backgroundColor: 'white',
        flex: 1.3,
        borderRadius: width / 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 0.5,
        borderColor: color.grey
    },
    infoStyle: {
        fontFamily: font.primary_mont,
        fontSize: 13,
        color:color.darkgrey,
        backgroundColor: 'transparent'
    },
    distanceContainer: {
        flex: 0.6,
        alignItems: 'flex-end',
        paddingRight: 6
    },
    distanceStyle: {
        flex: 1,
        backgroundColor: 'transparent',
        fontFamily: font.primary_mont_medium,
        fontSize: 14
    }
});

export default NearbyPlace;
