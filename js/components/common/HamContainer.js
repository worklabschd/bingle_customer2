import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Dimensions,
    Image
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { font } from '../theme';

const { height, width } = Dimensions.get('window');

const HamContainer = ({
    title, navigate, children, backEnabled, rightIcon, iconColor, iconDisplay, switchView
}) => {
    const {
        container,
        ham,
        heading
    } = styles;
    return (
        <View style={container} >
            <View style={{ position: 'absolute' }}>
                <TouchableOpacity
                    hitSlop={{top: 30, bottom: 30, left: 30, right: 30}}
                    onPress={() => { navigate(backEnabled ? '' : 'DrawerToggle'); }}
                    style={ham}
                >
                    <Ionicons
                        name={backEnabled ? 'ios-arrow-round-back' : 'md-menu'}
                        size={30}
                        color={iconColor ? '#00D6ff' : '#000000'}
                    />
                </TouchableOpacity>
            </View>
            <Text style={heading}>
                {title}
            </Text>
            <TouchableOpacity
                onPress={() => { rightIcon ? navigate('ListPlaces',rightIcon) : switchView ? navigate('Dashboard') : null }}
                style={{alignSelf:'flex-end', position:'absolute' ,width: 40, top:height / 25,backgroundColor: 'transparent'}}
            >
            { iconDisplay=="image" ? <Image source={require('../../../assets/icons/Street_View.png')} /> :
                <Ionicons
                    name={iconDisplay || null}
                    size={30}
                    color={iconColor ? '#00D6ff' : '#000000'}
                /> }
            </TouchableOpacity>
            <View style={{ flex: 1, marginTop: height / 17 }}>
                {children}
            </View>
        </View>
    );
};

HamContainer.propTypes = {
    title: PropTypes.string.isRequired,
    navigate: PropTypes.func.isRequired,
    backEnabled: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.element),
        PropTypes.element
    ]).isRequired
};

HamContainer.defaultProps = {
    backEnabled: false
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    ham: {
        top: height / 25,
        left: width / 15,
        width: 40,
        backgroundColor: 'transparent'
    },
    heading: {
        alignSelf: 'center',
        fontFamily: font.primary_mont,
        fontSize: 16,
        marginTop: 8.5,
        top: height / 25
    }
});

export default HamContainer;
