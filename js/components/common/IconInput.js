import React from "react";
import PropTypes from "prop-types";
import { View, StyleSheet, TextInput, Platform } from "react-native";
import * as Animatable from "react-native-animatable";
import { font } from "../theme";

export default class IconInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(value) {
    this.setState({ value });
    this.props.onChangeText(value);
  }

  setText(value) {
    this.setState({
      value
    });
  }

  render() {
    const { container, inputStyle, innerContainer, iconStyle } = styles;
    return (
      <Animatable.View ref={this.props.viewRef} style={container}>
        <View style={{ flex: 0.06 }} />
        <View style={innerContainer}>
          <TextInput
            ref={this.props.inputRef}
            placeholderTextColor="white"
            placeholder={this.props.placeholder}
            style={[inputStyle, !this.props.image ? { right: 0 } : null]}
            returnKeyType="done"
            multiline={false}
            clearButtonMode="while-editing"
            value={this.state.value}
            onChangeText={this.onChangeText}
            keyboardType={this.props.inputType}
            secureTextEntry={this.props.isPassword}
            onSubmitEditing={this.props.onSubmit}
            editable={this.props.editable}
            underlineColorAndroid="transparent"
          />
          {this.props.image ? (
            <View style={iconStyle}>{this.props.image}</View>
          ) : null}
        </View>
        <View style={{ flex: 0.06 }} />
      </Animatable.View>
    );
  }
}

IconInput.propTypes = {
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func,
  onSubmit: PropTypes.func,
  image: PropTypes.element,
  inputType: PropTypes.string,
  isPassword: PropTypes.bool,
  inputRef: PropTypes.func,
  viewRef: PropTypes.func,
  editable: PropTypes.bool
};

IconInput.defaultProps = {
  placeholder: "",
  inputType: "default",
  onChangeText: () => {},
  onSubmit: () => {},
  inputRef: () => {},
  viewRef: () => {},
  isPassword: false,
  image: null,
  editable: true
};

const styles = StyleSheet.create({
  container: {
    height: 30,
    flex: 1,
    flexDirection: "row",
    backgroundColor: "red"
  },
  innerContainer: {
    height: 30,
    flex: 1,
    borderBottomWidth: 1,
    borderColor: "white",
    flexDirection: "row"
  },
  inputStyle: {
    position: "absolute",
    right: 30,
    left: 0,
    bottom: Platform.OS !== "android" ? 5 : 3,
    top: 0,
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 20,
    flex: 1,
    fontFamily: font.primary,
    color: "white"
  },
  iconStyle: {
    position: "absolute",
    height: 22,
    width: 22,
    right: 7.5,
    bottom: Platform.OS !== "android" ? 10 : 6
  }
});
