import React from 'react';
import PropTypes from 'prop-types';
import { LinearGradient } from 'expo';
import { StyleSheet, StatusBar, Platform } from 'react-native';
import { color } from '../theme';
import {Image,ImageBackground} from 'react-native';

const MeegoBackground = (props) => (
    <ImageBackground
        source={require('../../../assets/images/background.jpg')}
        style={styles.container}
    >
        <StatusBar
            backgroundColor={Platform.OS === 'android' ? color.maroon : 'transparent'}
            barStyle="light-content"
        />
        {props.children}
    </ImageBackground>
);

MeegoBackground.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array
    ]).isRequired
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    }
});

export default MeegoBackground;
